/*
 *           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  ~~~~~~~~[ SARR Application Core modules ]~~~~~~~~~~
 *           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

/**
 * @namespace SARR
 * @type {*|{}}
 */
var SARR = SARR || {};


/*
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ SARR: Articles (Model) ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/**
 * Articles (Model). Contains methods for manipulating with the set of articles.
 * @class Articles
 * @namespace SARR
 * @type {*|Function|{}}
 */
SARR.Articles = SARR.Articles || {};


    /**
     * Constructor for creating new set of articles
     * @param $wrapper (jQuery element)
     * @constructor
     */
	SARR.Articles = function($wrapper) {
		this.$wrapper = $wrapper;
		this.articleIdList = [];
		this.articleList   = [];
        this.unreadArticlesCount= 0;
        this.starredArticlesCount=0;
	};


    /*
        Methods:
     */
	SARR.Articles.prototype = {

        /**
         * Remove all articles
         */
        removeAllArticles : function() {
            this.$wrapper.empty();
            this.articleIdList = [];
            this.articleList   = [];
            this.setUnreadArticleCount(0);
            this.setStarredArticleCount(0);
        },

        setUnreadArticleCount : function(number) {
            this.unreadArticlesCount = number;
            $('#unreadItemsCount').text(number).fadeTo('fast', 0.5).fadeTo('fast', 1.0);
        },

        refreshUnreadArticles : function() {
            var unreadCount = 0;
            $.each(this.articleList, function() {
                if (!this.isRead) unreadCount++;
            });
            this.setUnreadArticleCount(unreadCount);
        },

        setStarredArticleCount : function(number) {
            this.starredArticlesCount = number;
            $('#starredItemsCount').text(number).fadeTo('fast', 0.5).fadeTo('fast', 1.0);
        },

        refreshStarredArticles : function() {
            var starredCount = 0;
            $.each(this.articleList, function() {
                if (this.isStarred) starredCount++;
            });
            this.setStarredArticleCount(starredCount);
        },


        /**
         * @param {SARR.Article} article article object (SARR.Article)
         */
        appendArticle : function(article) {
//                console.log(Log.info('appendArticle() > article '+article.id));

                if (this.articleIdList.contains(article.id)) {
                    console.error(Log.err('.appendArticle() > article with id: ' +article.id+ ' is in article list'));
                    console.warn(article);
                }
                else {
                    this.articleIdList.push(article.id);
                    this.articleList.push(article);
                    this.$wrapper.append( article.asElement() );
                }
        },

        /**
         * @param articlesJson array of article object props
         */
        appendAllArticlesFromJson : function(articlesJson, articles) {
                this.removeAllArticles();

                var articleObj = null;
                for (var i=0; i<articlesJson.length; i++) {
                    articleObj = articlesJson[i];
//                    console.log(Log.info('appendAllArticlesFromJson() > article ' + articleObj.id));
                    this.appendArticle(SARR.Article.createArticleFromProps(articleObj, articles));
                }


            this.refreshUnreadArticles();
            this.refreshStarredArticles();
            console.log(this);

        },

        /**
         * Returns @link SARR.Article object for
         * @param $article element of article <code> &lt;article&gt; ... &lt;/article&gt; </code>
         * @returns {SARR.Article|null}
         */
        getArticleByElement : function($article) {
                for (var article in this.articleList) {
                    if (this.articleList.hasOwnProperty(article)) {

                        if (article.id === $article.getAttribute('id')) {
                            return article;
                        }
                    }
                }
                return null;
        },

        setTagsFunctionality : function(jsonTags) {
            (function setTagsInput (SARRarticles) {
                try {
                    var tagInputArray = SARRarticles.$wrapper.find('article>header input');
                    var tags = jsonTags;

                    tagInputArray.each(function() {
                        var $tagsInput = $(this);

                        /*
                             Transform all tags elements to special Bootstrap Tags Input
                             http://timschlechter.github.io/bootstrap-tagsinput/examples/bootstrap3/
                         */
                        $tagsInput.tagsinput();
                        $tagsInput.tagsinput('input').typeahead(
                            {
                                minLength: 0,
                                items: 16,
                                name: 'tags',
                                local: tags
                            }
                        ).bind('typeahead:selected', $.proxy(function (obj, datum) {
                                this.tagsinput('add', datum.value);
                                this.tagsinput('input').typeahead('setQuery', '');
                            }, $tagsInput)
                        );

                        /*
                            Add tags to button FilterByTags
                         */
                        var $refreshOnlyTagsList = $('#refreshOnlyTagsList');
                        $refreshOnlyTagsList.empty();
                        $.each(tags, function() {
                            $refreshOnlyTagsList.append( $('<li><a>'+ this +'</a></li>') );
                        });

                        /*
                            Add tags to Bootstrap Tags Input dropdown element
                         */
                        var $dropdown = $tagsInput.parent().find('.tt-dropdown-menu');
                        $tagsInput.parent().find('.twitter-typeahead > input').on('click', function() {
                            $dropdown.show();
                            var allTags = '';
                            $.each(tags, function() {
                                allTags += '' +
                                    '<div class="tt-suggestion" style="white-space: nowrap; cursor: pointer;">' +
                                        '<p style="white-space: normal;">' + this +  '</p>' +
                                    '</div>';
                            });
                            $dropdown.html(
                                $('<div class="tt-dataset-tags" style="display: block;">' +
                                    '<span class="tt-suggestions" style="display: block;">' +
                                        allTags +
                                    '</span>' +
                                 '</div>')
                            );
                            $dropdown.find('.tt-suggestion').click(function() {
                                var tagValue = $(this).find('p').text();
                                // save tag
                                $tagsInput.tagsinput('add', tagValue);
                            });
                        });
                        $tagsInput.parent().find('.tt-query').on('blur', function() {
                            $dropdown.fadeOut();
                        });
                    });


                } catch (e) {
                    console.warn(Log.err('TODO: Tags are not implemented !!!!!!! ' + e));
                }
            })(this);
        }


	};






/*
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ SARR: Article ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/**
 * This "class" represents article.
 * @class Article
 * @namespace SARR
 * @type {*|Function|{}}
 */
SARR.Article = SARR.Article || {};


    /**
     * Constructor of article. This builds an article object.
     * @param props object of article data. For example:
     * <pre>
     *  {
	 *		id		: 0,
	 *		name 	: "Lukášov článok",
	 *		updated : "2013-10-08T20:26+02:00",
	 *		link	: "https://www.google.com/search?q=lukas",
	 *		source  : {
	 *			name: "lukas.sk",
	 *			link: "#lukas.sk"
	 *		},
	 *		isStarred : true,
	 *		seen	: true,
	 *		shortText: "Takto začína článok, pretože takto je to fajn. Iba, že by to tak nebolo fajn",
	 *	    longText: "Obsah článku"
	 *	}
     * </pre>
     * @constructor
     * TODO shortText je vlastne kratsia verzia longTextu bez HTML znaciek PREROBIT!!!!!!
     */
    SARR.Article = function(props, articles) {
        this.$element   = null;
        this.id 	    = props.id;
        this.name 	    = props.title;
        this.tags	    = props.tags;
        this.source     = props.source;
        this.isRead     = props.seen;
        this.isStarred  = props.starred;
        this.datetime   = new Date(props.publishDate);
        this.link 	    = props.link;
        this.shortText  = props.description.replace(/<\/?[^>]+(>|$)/g, "");
        this.longText   = props.description;
        this.encodedLinkURL = encodeURIComponent(this.link);
        this.articles   = articles;
    };


    /**
     * Generates article object to html element (view)
     * @returns jQuery element
     */
	SARR.Article.prototype.asElement = function() {
		var thisArticle = this;
        var $articleElement = this.create$element();
		Utils.Article.humanizeDate($articleElement);

		/*
		    Opening/closing article
		     [!] .bind(this) na konci je tam preto, aby this vo volanej metode openingAndClosing
		         neukazovalo na element <header>, ale na normalne this > objekt Article()
		 */
		$( $articleElement.find('header') ).click( this.openingAndClosing.bind(this) );

        $articleElement.find('div.entry-content').each(function() {
            $(this).removeClass('hide');
            $(this).hide();
        });


        /*
            Tags changes - sent to server
         */
        var $tagsInput = $articleElement.find('.tags input');
        $tagsInput.change(function(ev) {
            var newTagsArray = $(ev.target).val().split(',');
            thisArticle.changeTags($tagsInput, newTagsArray);
        });

        /*
            Click on the star
         */
		$articleElement.find('.star').click( function(e) {
			e.preventDefault();
			thisArticle.toggleStar();
		});

        /*
            Click on the Pocket (icon)
         */
		$articleElement.find('.pocket').click( function(e) {
			e.preventDefault();
            Utils.ExternalApps.pocketPopup(e);

		});

        /*
            Click on the Facebook (icon)
         */
        $articleElement.find('.facebook').click( function(e) {
            e.preventDefault();
            FB.ui(
                {
                    method: 'feed',
                    link: e.target.getAttribute("data-url"),
                    caption: e.target.getAttribute("data-title")
                }
            );
        });


        /*
         * Ošetrenie zatvorenia otvoreného článku pri kliknutí do oblasti tagov
         */
        $articleElement.find(".noOpenContent").on( 'click', function(e) {
            e.stopPropagation();
        });

		this.$element = $articleElement;
		return $articleElement;
	};



	SARR.Article.prototype.setRead = function(isRead) {
        var thisArticle = this;
		if (isRead && !this.isRead) {

            var markReadRoute = jsRoutes.controllers.Articles.markRead(this.id);

            $.ajax({
                type: markReadRoute.type,
                url:  markReadRoute.url,
                data: {  }
            })
                .done(function( msg ) {
                    thisArticle.isRead = isRead;
                    thisArticle.$element.removeClass('unread');
                    console.log(Log.info('Article (id: ' +thisArticle.id+ ') is now read.'));
                    thisArticle.articles.refreshUnreadArticles();
                })
                .fail(function (jqxhr, textStatus, error) {
                    console.error(Log.err("Request Failed: " + textStatus + ", " + error));
                });
		}
	};


	SARR.Article.prototype.toggleStar = function() {
        var thisArticle  = this;
        var $elementStar = thisArticle.$element;
        var isStarred    = thisArticle.isStarred;
        var newStarValue = !isStarred;
        var changeStarRoute = jsRoutes.controllers.Articles.addStar(this.id, newStarValue);

        var ajax = $.ajax({
            type: changeStarRoute.type,
            url:  changeStarRoute.url,
            data: {  }
        });

        if (!isStarred) {
            ajax.done(function( msg ) {
                thisArticle.isStarred = !thisArticle.isStarred;
                $elementStar.addClass('starred');
                console.log(Log.info('Article (id: ' +thisArticle.id+ ') is now isStarred.'));
                thisArticle.articles.refreshStarredArticles();
            })
                .fail(function (jqxhr, textStatus, error) {
                    console.error(Log.err("Request Failed: " + textStatus + ", " + error));
                });

        }
		else {
            ajax.done(function( msg ) {
                thisArticle.isStarred = !thisArticle.isStarred;
                $elementStar.removeClass('starred');
                console.log(Log.info('Article (id: ' +thisArticle.id+ ') is now unstarred.'));
                thisArticle.articles.refreshStarredArticles();
            })
                .fail(function (jqxhr, textStatus, error) {
                    console.error(Log.err("Request Failed: " + textStatus + ", " + error));
                });
		}
	};


    SARR.Article.prototype.changeTags = function($tagsInput, newTagsArray) {
        var thisArticle = this;
        var tagsArray = newTagsArray;

//        console.log('this.tags    = ' + this.tags);
//        console.log('newTagsArray = ' + newTagsArray);

        if (this.tags.compare(tagsArray)) {
            return;
        }
        var tagsJSON = JSON.stringify(tagsArray);
        var changeTagsRoute = jsRoutes.controllers.Tags.addTags(this.id);

        $.ajax({
            type: changeTagsRoute.type,
            url:  changeTagsRoute.url,
            data: { tags: tagsJSON }
        })
            .done(function( msg ) {
                thisArticle.tags = tagsArray;
                console.log(Log.info('Article (id: ' +thisArticle.id+ ') has now tags: ' + tagsJSON));
            })
            .fail(function( jqXHR, textStatus ) {
                console.log(Log.info('"Request failed: " + textStatus'));
            });
    };


    SARR.Article.prototype.openingAndClosing = function(e) {
        var ANIMATION_SPEED = (window.SARRanimations) ? 200 : 0;

        e.preventDefault();
        var ACTIVE = 'active';

        var $articleElement = this.$element;
        var $articleHeader  = $articleElement.find('> header');
        var $articleBody = $articleElement.find('> div.entry-content');

        this.setRead( $articleElement.hasClass('unread') );

        if ($articleHeader.hasClass(ACTIVE)) {
            $articleBody.slideUp(ANIMATION_SPEED);
            $articleHeader.removeClass(ACTIVE);
        }
        else {  //close all
            var $articleContainer = $('#mainContent');
            $articleContainer.find('> article > header.active' ).removeClass( ACTIVE );
            $articleContainer.find('> article > div.entry-content' ).hide();

            $articleHeader.addClass( ACTIVE );
            $articleBody.slideDown(ANIMATION_SPEED);
        }
    };



	SARR.Article.createArticleFromProps = function(articleObj, articles) {
		return new SARR.Article( articleObj, articles );
	};



    SARR.Article.prototype.create$element = function() {
        var unreadClass = (this.isRead) ? '' : 'unread';
        var starred  	= (this.isStarred) ? 'starred' : '';

        var vgoogleShare = "https://plus.google.com/share?url=" + this.link + "/";
        this.longText = this.longText.replace(/&lt;/g,'<').replace(/&gt;/g,'>');

        var tagsCommaSepared = "";
        for (var i=0; i<this.tags.length; i++) {
            tagsCommaSepared += this.tags[i] + ((i !== this.tags.length -1) ? ',' : '');
        }
//        console.log(Log.info('tagsCommaSepared = ' + tagsCommaSepared));

        var $articleElement = $(
            '<article id="' +this.id+'" class="' +unreadClass+ ' ' +starred+ '"> \
                    <header> \
                        <h2>' +this.name+ '</h2> \
                        <span class="star left icon-star-empty noOpenContent" title="toggle favorite"></span> \
                        <a class="source left noOpenContent" target="_blank" \
                            href="' +this.source.link+ '"> \
                            ' +this.source.title+ ' \
                        </a> \
                        <time class="updated right" \
                            datetime="' +this.datetime+ '" pubdate> \
                        </time> \
                        <a class="externalLink icon-external-link noOpenContent" \
                            title="open article in new tab" target="_blank" \
                            href="' +this.link+ '"> \
                        </a> \
                        <span class="begin"> \
                            ' +this.shortText+ ' \
                        </span> \
                     \
                     \
                            <aside class="tagsContainer noOpenContent"> \
                                <span class="tags icon-tags "> \
                                    <input type="text" value="' +tagsCommaSepared+ '" data-role="tagsinput" placeholder="+" />	\
                                </span> \
                            </aside> \
                    </header> \
                     \
                    <div class="entry-content ovf hide"> \
                    \
                        <div class="articleWrapper left"> \
                            <aside class="sharing right"> \
                                <ul> \
                                    <li><a class="shareFriend icon-user" href="#"> </a></li> \
                                    <li><a class="shareEmail  icon-envelope" href="#"> </a></li> \
                                    <li><a class="shareEvent  icon-calendar-empty" href="#"></a></li> \
                                    <li><a class="shareGooglePlus icon-google-plus-sign left" target="_blank"  href="' + vgoogleShare +'"></a> \
                                        <a class="shareFacebook   icon-facebook-sign facebook   left" href="" data-url="' +this.link+ '\" data-title="' +this.name+ '"  \
                                            title="Share on Facebook" onlick="return false;"></a> \
                                        <a class="shareTwitter    icon-twitter-sign     left"></a> </li> \
                                    <li><a class="sharePocket     icon-pocket pocket" href="" data-url="' +this.link+ '\" data-title="' +this.name+ '"  \
                                            title="Add to Pocket" onlick="return false;"> </a> </li> \
                                    <li><a class="shareOther  icon-share" href="#"> </a></li> \
                                </ul> \
                            </aside> \
                     \
                            <div class="articleContent"> \
                            	<div> \
                            		' + this.longText + '		\
                            	</div> \
                            </div> \
                     \
                            <a class="openArticle icon-external-link" href="' +this.link+ '" target="_blank"> Go to article</a> \
                    \
                        </div><!--.articleWrapper--> \
                    </div><!--.entry-content--> \
                </article>'
        );
        return $articleElement;
    };





