/**
 *   MAIN SARR APP
 *  ===============
 */
;(function($, undefined){
$(document).ready(function() {


    /** GLOBAL settings **/
    window.SARRvisualLog  = true;
    window.SARRanimations = true;
    window.SARRresizeDesc = true;

	// this object represnts abstract layer for list of articles
	// constructor's argument is jQuery element object
	// which is wrapper for all articles

	var articles = new SARR.Articles($('#mainContent'));

    var allArticlesRoute  = jsRoutes.controllers.Articles.listAsJSON();
    var allUnreadArticles = jsRoutes.controllers.Articles.listNotRead();
    var allStarredArticles= jsRoutes.controllers.Articles.listStarred();
    var addSourceRoute    = jsRoutes.controllers.Sources.addSource();

    var $articlesMenuButtons = $('#articlesMenuButtons');
    var $articlesMenuButtonsAll = $articlesMenuButtons.find('.btn');
    var $refreshArticlesBtn = $('#refreshArticles');
    var $refreshUnreadArticlesBtn = $('#refreshOnlyUnread');
    var $refreshStarredArticlesBtn = $('#refreshOnlyStarred');
    var $addSourceBtn = $('#addSource');

    loadAllArticles(allArticlesRoute);


    function loadAllArticles(route, data) {
        var postData = data || {};
        articles.$wrapper.hide();
        $articlesMenuButtonsAll.addClass('disabled');
        $('#loadingAnimation').show();

        $.ajax({
            type: route.type,
            url:  route.url,
            data: postData,
            success: function (response, textStatus, xhr) {
                if (Utils.AJAX.checkResponseToLogOut(response)) return;
                var json = response;
                loadTags(
                    function () {
                        articles.appendAllArticlesFromJson(json, articles);
                        Utils.After.resizeShortText();
                        $('#loadingAnimation').hide();
                        $articlesMenuButtonsAll.removeClass('disabled');
                        articles.$wrapper.show();
                    },
                    function () {
                        setButtonsListeners();
                    });
            },
            fail: function (jqxhr, textStatus, error) {
                console.error(Log.err("Request Failed: " + textStatus + ", " + error));
                $('#loadingAnimation').hide();
            }
        });
    }


    function loadTags(functionBefore, functionAfter) {

        var articlesObj = articles;
        var tagListRoute = jsRoutes.controllers.Tags.listAsJSON();

        // get tags
        $.getJSON(tagListRoute.url)
            .done(function (jsonTags) {
                if (Utils.AJAX.checkResponseToLogOut(jsonTags)) return;
                functionBefore();
                articlesObj.setTagsFunctionality(jsonTags);
                functionAfter();
            })

            .fail(function (jqxhr, textStatus, error) {
                console.error(Log.err("Request Failed: " + textStatus + ", " + error));
            }
        );
    }

    function setButtonsListeners() {
        $refreshArticlesBtn.unbind('click').click(        function() { console.log(Log.info('View all'));        loadAllArticles(allArticlesRoute); } );
        $refreshUnreadArticlesBtn.unbind('click').click(  function() { console.log(Log.info('Refresh unread'));  loadAllArticles(allUnreadArticles); } );
        $refreshStarredArticlesBtn.unbind('click').click( function() { console.log(Log.info('Refresh starred')); loadAllArticles(allStarredArticles); } );
        $addSourceBtn.unbind('click').click( function() { addSource(addSourceRoute); } );

        /*
            Add tags to button FilterByTags
         */
        var $refreshOnlyTagsList = $('#refreshOnlyTagsList');

        $refreshOnlyTagsList.find('a').unbind('click').click(function(e) {
            var tag = $(this).html();
            console.log(Log.info( 'Filter by tag "' + tag + '"'));
            var tagFilterRoute = jsRoutes.controllers.Articles.listWithTag();
            loadAllArticles(tagFilterRoute, {tag: tag});
        });
    }


    function addSource(route) {
        alertify.prompt('Add source URL (for example: http://www.zive.sk/rss/sc-47/default.aspx )', function(isButtonOK, inputStringURL) {
            if (isButtonOK) {

                if (!inputStringURL.isValidURL()) {
                    console.warn(Log.err("It's not a valid URL!"));
                    return;
                }

                $.ajax({
                    type: route.type,
                    url:  route.url,
                    data: { url: inputStringURL }
                })
                    .done(function( msg ) {
                        console.log(Log.ok('Source URL="' + msg + '" was added.'));
                    })
                    .fail(function (jqxhr, textStatus, error) {
                        if (jqxhr.status === 500)
                            console.error(Log.err("Wrong source URL or no feed found or source already exists."));
                        else console.error(Log.err("Request Failed: " + textStatus + ", " + error));
                    });

            } else {
                console.warn(Log.err('Adding route cancelled.'));
            }
        });
    }

});
}).call(SARR, jQuery);
