# Ako vyskúšať u seba lokálne SARR (iba klientska časť) #
-----------------------

1. Mať nainštalovaný [Node.js](http://nodejs.org/download/)
2. V Node.js mať nainštalovaný balíček ```http-server ``` ([link](https://npmjs.org/package/http-server))

    príkaz na inštaláciu: ```npm install -g http-server```

    (prepínač -g preto, aby to bolo dostupné globálne - z príkaz. riadku)

3. V priečinku ```SARR\public\_StaticSARR\old``` otvoriť príkazový riadok a spustiť príkaz ```http-server -p 80```
   (spustí server na porte 80, na adrese http://localhost/ bude stránka)
