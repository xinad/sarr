/**
 *   MAIN SARR APP
 *  ===============
 */
;(function($, undefined){
$(document).ready(function() {


	// this object represnts abstract layer for list of articles
	// constructor's argument is jQuery element object
	// which is wrapper for all articles

	var articles = new SARR.Articles($('#mainContent'));


	// create dummy articles:

	var articleLukas = new SARR.Article( 
		{
			id		: 0,
			name 	: "Lukášov článok",
			updated : "2013-10-08T20:26+02:00",
			link	: "https://www.google.com/search?q=lukas",
			source  : {
				name: "lukas.sk",
				link: "#lukas.sk"
			},
			starred : false,
			read	: true,
			shortText: "Takto začína článok, pretože takto je to fajn. Iba, že by to tak nebolo fajn"
		}
	);
	var articleFilip = new SARR.Article( 
		{
			id		: 1,
			name 	: "Filipov článok",
			updated : "2013-10-08T20:00+02:00",
			link	: "https://www.google.com/search?q=Filip",
			source  : {
				title: "filip.sk",
				link: "#filip.sk"
			},
			starred : false,
			read	: false,
			shortText: "Pôvod mena z gréckeho Philos+hippos, čo znamená milovník koní. Stretnúť muža menom Filip, "
		}
	);
	var articleBadger = new SARR.Article( 
		{
			id		: 2,
			name 	: "Borsuk, to je jazvec",
			updated : "2013-10-05T20:26+02:00",
			link	: "http://youtube.com/watch?v=EIyixC9NsLI",
			source  : {
				title: "badgerbadgerbadger.com",
				link: "#badger.com"
			},
			starred : true,
			read	: true,
			shortText: "Jazvec lesný alebo jazvec obyčajný alebo (?) jazvec hôrny (lat. Meles meles) je cicavec z čeľade lasicovité.  V užšom (novšom) zmysle je to druh vyskytujúci sa len v Európe"
		}
	);


//	//append articles
//
//	articles.appendArticle(articleLukas);
//	articles.appendArticle(articleFilip);
//	articles.appendArticle(articleBadger);
//
//	//error because articleLukas is already appended
//	articles.appendArticle(articleLukas);


	// get info about articles object to console
	console.debug(articles);


    $.getJSON("js/json.json")
    
        .done(function (json) {

            articles.appendAllArticlesFromJson(json);
            Utils.After.resizeShortText();
            $('#animatedCircles').hide();
            // setTimeout(function(){
            // 	articles.removeAllArticles();
            // }, 5000);
        })

        .fail(function (jqxhr, textStatus, error) {

            var err = textStatus + ", " + error;
            console.error(Log.err("Request Failed: " + err));

        }
    );





	//Utils.After.resizeShortText();
});
}).call(SARR, jQuery);
