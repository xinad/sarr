/*
 *           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  ~~~~~~~~[ SARR Application Core modules ]~~~~~~~~~~
 *           ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

/**
 * @namespace SARR
 * @type {*|{}}
 */
var SARR = SARR || {};


/*
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ SARR: Articles (Model) ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/**
 * Articles (Model). Contains methods for manipulating with the set of articles.
 * @class Articles
 * @namespace SARR
 * @type {*|Function|{}}
 */
SARR.Articles = SARR.Articles || {};


    /**
     * Constructor for creating new set of articles
     * @param $wrapper (jQuery element)
     * @constructor
     */
	SARR.Articles = function($wrapper) {
		this.$wrapper = $wrapper;
		this.articleIdList = [];  // = new Array();
		this.articleList   = [];
	};


    /*
        Methods:
     */
	SARR.Articles.prototype = {

        /**
         * @param {SARR.Article} article article object (SARR.Article)
         */
        appendArticle : function(article) {
                console.log(Log.info('appendArticle() > article '+article.id));

                if (this.articleIdList.contains(article.id)) {
                    console.error(Log.err('.appendArticle() > article with id: ' +article.id+ ' is in article list'));
                    console.warn(article);
                }
                else {
                    this.articleIdList.push(article.id);
                    this.articleList.push(article);
                    this.$wrapper.append( article.asElement() );
                }
        },

        /**
         * @param articlesJson array of article object props
         */
        appendAllArticlesFromJson : function(articlesJson) {
                var articleObj = null;
                for (var i=0; i<articlesJson.length; i++) {
                    articleObj = articlesJson[i];
                    console.log(Log.info('appendAllArticlesFromJson() > article ' + articleObj.id));
                    this.appendArticle(SARR.Article.createArticleFromProps(articleObj));
                }
        },

        /**
         * Returns @link SARR.Article object for
         * @param $article element of article <code> &lt;article&gt; ... &lt;/article&gt; </code>
         * @returns {SARR.Article|null}
         */
        getArticleByElement : function($article) {
                for (var article in this.articleList) {
                    if (this.articleList.hasOwnProperty(article)) {

                        if (article.id === $article.getAttribute('id')) {
                            return article;
                        }
                    }
                }
                return null;
        },

        /**
         * Remove all articles
         */
        removeAllArticles : function() {
                this.articleIdList = [];
                this.articleList   = [];
                this.$wrapper.empty();
        }

	};






/*
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ SARR: Article ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/**
 * This "class" represents article.
 * @class Article
 * @namespace SARR
 * @type {*|Function|{}}
 */
SARR.Article = SARR.Article || {};


    /**
     * Constructor of article. This builds an article object.
     * @param props object of article data. For example:
     * <pre>
     *  {
	 *		id		: 0,
	 *		name 	: "Lukášov článok",
	 *		updated : "2013-10-08T20:26+02:00",
	 *		link	: "https://www.google.com/search?q=lukas",
	 *		source  : {
	 *			name: "lukas.sk",
	 *			link: "#lukas.sk"
	 *		},
	 *		starred : true,
	 *		read	: true,
	 *		shortText: "Takto začína článok, pretože takto je to fajn. Iba, že by to tak nebolo fajn",
	 *	    longText: "Obsah článku"
	 *	}
     * </pre>
     * @constructor
     */
    SARR.Article = function(props) {
        this.$element   = null;
        this.id 	    = props.id;
        this.name 	    = props.name;
        this.source     = props.source;
        this.tags	    = props.tags;
        this.isRead     = props.read;
        this.starred    = props.starred;
        this.datetime   = props.updated;
        this.link 	    = props.link;
        this.shortText  = props.shortText;
        this.longText   = props.longText;
        this.encodedLinkURL = encodeURIComponent(this.link);
    };


    /**
     * Generates article object to html element (view)
     * @returns jQuery element
     */
	SARR.Article.prototype.asElement = function() {
		var thisArticle = this;
        var $articleElement = this.create$element();
		Utils.Article.humanizeDate($articleElement);

		/*
		    Opening/closing article
		     [!] .bind(this) na konci je tam preto, aby this vo volanej metode openingAndClosing
		         neukazovalo na element <header>, ale na normalne this > objekt Article()
		 */
		$( $articleElement.find('header') ).click( this.openingAndClosing.bind(this) );

        $articleElement.find('div.entry-content').each(function() {
            $(this).removeClass('hide');
            $(this).hide();
        });

        /*
            Click on the star
         */
		$articleElement.find('.star').click( function(e) {
			e.preventDefault();
			thisArticle.toggleStar();
		});

        /*
            Click on the Pocket (icon)
         */
		$articleElement.find('.pocket').click( function(e) {
			e.preventDefault();
            Utils.ExternalApps.pocketPopupBetter(e);
            
		});

        /*
         * Ošetrenie zatvorenia otvoreného článku pri kliknutí do oblasti tagov
         */
        $articleElement.find(".noOpenContent").on( 'click', function(e) {
            e.stopPropagation();
        });

		this.$element = $articleElement;
		return $articleElement;
	};



	SARR.Article.prototype.setRead = function(isRead) {
		if (isRead && !this.isRead) {
			this.isRead = isRead;
			this.$element.removeClass('unread');
			console.log(Log.info('Article (id: ' +this.id+ ') is now read.'));
		}
		else if (!isRead && this.isRead) {
			this.isRead = isRead;
			this.$element.addClass('unread');
			console.log(Log.info('Article (id: ' +this.id+ ') is now unread.'));	
		}
	};


	SARR.Article.prototype.toggleStar = function() {
		this.starred = !this.starred;
		if (this.starred) {
			this.$element.addClass('starred');
		}
		else {
			this.$element.removeClass('starred');
		}
		console.log(Log.info('Article (id: ' +this.id+ ') is now ' +((!this.starred) ? 'un':'')+ 'starred.'));
	};


    SARR.Article.prototype.openingAndClosing = function(e) {

        e.preventDefault();
        var SHOW = 'show',  CLOSED = 'hide',  ACTIVE = 'active';

        this.setRead(true);

        var $articleElement = this.$element;
        var $articleHeader  = $articleElement.find('> header');

        var $articleBody = $articleElement.find('> div.entry-content');
        if ($articleHeader.hasClass(ACTIVE)) {
//				$articleBody.hide();
            $articleBody.slideUp(200);
//				$articleBody.removeClass( SHOW );
//				$articleBody.addClass( CLOSED );
            $articleHeader.removeClass(ACTIVE);
        }
        else {
            //close all
            var $articleContainer = $('#mainContent');
//				$articleContainer.find('> article > div.entry-content' ).hide();
//				$articleContainer.find('> article > header.active' ).parent().find('div.entry-content').removeClass( SHOW );
//				$articleContainer.find('> article > header.active' ).parent().find('div.entry-content').addClass( CLOSED );
            $articleContainer.find('> article > header.active' ).removeClass( ACTIVE );
            $articleContainer.find('> article > div.entry-content' ).hide();

            $articleHeader.addClass( ACTIVE );
//                $articleBody.addClass( SHOW );
//                $articleBody.removeClass( CLOSED );
            $articleBody.slideDown(200);
//				$articleBody.fadeIn(500);
        }
    };



	SARR.Article.createArticleFromProps = function(articleObj) {
		return new SARR.Article( articleObj );
	};



    SARR.Article.prototype.create$element = function() {
        var unreadClass = (this.isRead) ? '' : 'unread';
        var starred  	= (this.starred) ? 'starred' : '';
        var tagsLiElements = "";
        $.each(this.tags, function(index, value){
            tagsLiElements += '<li> <span class="tag">' +value+ '</span> <span class="icon-remove"></span> </li> \n';
        });

        this.longText = this.longText.replace(/&lt;/g,'<').replace(/&gt;/g,'>');

        var $articleElement = $(
            '<article id="' +this.id+'" class="' +unreadClass+ ' ' +starred+ '"> \
                    <header> \
                        <h2>' +this.name+ '</h2> \
                        <span class="star left icon-star-empty noOpenContent" title="toggle favorite"></span> \
                        <a class="source left noOpenContent" target="_blank" \
                            href="' +this.source.link+ '"> \
                            ' +this.source.title+ ' \
                        </a> \
                        <time class="updated right" \
                            datetime="' +this.datetime+ '" pubdate> \
                        </time> \
                        <a class="externalLink icon-external-link noOpenContent" \
                            title="open article in new tab" target="_blank" \
                            href="' +this.link+ '"> \
                        </a> \
                        <span class="begin"> \
                            ' +this.shortText+ ' \
                        </span> \
                     \
                     \
                            <aside class="tagsContainer noOpenContent"> \
                                <ul class="tags icon-tags "> \
                                    ' +tagsLiElements+'	\
                                </ul> \
                                <span class="addTag icon-plus "></span> <span>&nbsp;</span> \
                            </aside> \
                    </header> \
                     \
                    <div class="entry-content ovf hide"> \
                    \
                        <div class="articleWrapper left"> \
                            <aside class="sharing right"> \
                                <ul> \
                                    <li><a class="shareFriend icon-user" href="#"> </a></li> \
                                    <li><a class="shareEmail  icon-envelope" href="#"> </a></li> \
                                    <li><a class="shareEvent  icon-calendar-empty" href="#"></a></li> \
                                    <li><a class="shareGooglePlus icon-google-plus-sign left" href="#"></a> \
                                        <a class="shareFacebook   icon-facebook-sign    left" href="#"></a> \
                                        <a class="shareTwitter    icon-twitter-sign     left"></a> </li> \
                                    <li><a class="sharePocket     icon-pocket pocket" href="" data-url="' +this.link+ '\" data-title="' +this.name+ '" data-tags="' +this.tags.toString()+ '" \
                                            title="Add to Pocket" onlick="Utils.ExternalApps.pocketPopupBetter(event); return false;"> </a> </li> \
                                    <li><a class="shareOther  icon-share" href="#"> </a></li> \
                                </ul> \
                            </aside> \
                     \
                            <div class="articleContent"> \
                            	<div> \
                            		' + this.longText + '		\
                            	</div> \
                            </div> \
                     \
                            <a class="openArticle icon-external-link" href="' +this.link+ '" target="_blank"> Go to article</a> \
                    \
                        </div><!--.articleWrapper--> \
                    </div><!--.entry-content--> \
                </article>'
        );
        return $articleElement;
    };





