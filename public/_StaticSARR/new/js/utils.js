/**
 * Global improvements
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 9.10.2013 | Time: 19:04
 */


/**
 * Formats date object
 * Example of using:
 * <code> var string = new Date().format('YYYY-MM-dd hh:mm:ss,S'); </code>
 * @param format String
 * @returns {String} formatted date (String)
 */
Date.prototype.format = function (format) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1,//month
        "d+": this.getDate(),     //day
        "h+": this.getHours(),    //hour
        "m+": this.getMinutes(),  //minute
        "s+": this.getSeconds(),  //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};


/**
 * Sets length of string. Example:
 * <pre>
 *     var name = "Gžegož";  // name.length = 6
 *     name.setSize(9);      // name.length = 9; name === "Gžegož   "
 *     name.setSize(3);      // name.length = 3; name === "Gže"
 * </pre>
 * @param {number} n new length (int)
 * @returns {String} string with new length
 */
String.prototype.setSize = function (n) {
    var t = this;
    if (n < 0) return t;
    if (n <= t.length) return t.substr(0, n);
    else {
        var spaces = '';
        for (var i = 0; i < (n - t.length); i++) {
            spaces += ' ';
        }
        return t + spaces;
    }
};


/**
 * Classic contains method
 * @param obj
 * @returns {boolean} true/false
 */
Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
};





/**
 * Helper class for better logs with timestamp. Example of using:
 * <pre> console.log(Log.info("message for log")); </pre>
 * @class Log
 * @type {Function}
 */
var Log = Log || {};

Log = function abstractLog(str) {
    return new Date().format("hh:mm:ss,S ") + ' ' + str;
};

Log.info = function (str) {
    return '  [INFO]  ' + Log(str);
};

Log.err = function (str) {
    return '[ERROR] ' + Log(str);
};





/*
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ Utils namespace ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */


/**
 * Utils is namespace for helpful static methods.
 * @namespace Utils
 * @type {*|{}}
 */
var Utils = Utils || {};

/**
 * (Static) class Article in namespace Utils provides methods
 * for manipulating with Article
 * @class Article
 * @namespace Utils
 * @type {Article}
 */
Utils.Article = Utils.Article || {};


/**
 * Finds and changes date/time to format like "pred 3 hodinami"
 * @param $article article element (jQuery element)
 */
Utils.Article.humanizeDate = function ($article) {

    $article.find('.updated').each(function () {

        var newText = fromNow(this.getAttribute("datetime"));
        if (newText === null || newText === undefined || newText == "") {
            newText = fromNow(this.getAttribute("datetime"), null);
        }
        var dateInnerElement = this.querySelector("*");
        if (dateInnerElement !== null && dateInnerElement !== undefined) {
            dateInnerElement.innerHTML = newText;
        } else {
            this.innerHTML = newText;
            this.setAttribute("title", this.getAttribute("datetime"));
        }
    });

    function fromNow(date, format) {
        if (date.search('GMT') > -1) date = date.substring(0, date.search('GMT'));
        var momentResult = moment(date, format).fromNow();
        return momentResult =
            (momentResult === "pred dňom") ? "včera" :
                (momentResult === "pred 2 dňami") ? "predvčerom" : momentResult;
    }
};






/**
 * (Static) class After in namespace Utils provides methods
 * for executing as last (after main app)
 * @class After
 * @namespace Utils
 * @type {After}
 */
Utils.After = Utils.After || {};


/**
 *  Resize article beginning text (one line) after article title.
 *  Resizing is executed when browser is resizing and when article width is changed.
 */
Utils.After.resizeShortText = function () {

    /**
     *    NOTE: requires jQuery.resize plugin (http://benalman.com/projects/jquery-resize-plugin/)
     */
    function resizeShortText() {
        // console.debug('resize');

        $('#mainContent').find('> article').each(function () {
            var usedWidth = 0;
            var _this = $(this);
            usedWidth += _this.find('.star').outerWidth();
            usedWidth += _this.find('a.source').outerWidth();
            usedWidth += _this.find('time.updated').outerWidth();
            usedWidth += _this.find('.externalLink').outerWidth();
            usedWidth += _this.find('h2').outerWidth();

            var all = _this.width();
            var computedWidth = all - usedWidth - 60;

            _this.find('span.begin').css({width: computedWidth + 'px'});
        });
    }

    jQuery.resize.delay = 300;
    resizeShortText();

    /**
     *    NOTE: requires jQuery.resize plugin (http://benalman.com/projects/jquery-resize-plugin/)
     */
    $('#mainContent').find('> article > header').resize(function (e) {
        ($(window).width() > 900) && resizeShortText();
    });

    $('#mainContent').find('> article .source').hover(resizeShortText, resizeShortText);


    // $(window).resize(resizeShortText);
    // $(window).resize( function() {
    // 	($(window).width() > 760) && resizeShortText();
    // });

    // if (navigator.userAgent.search('Mozilla')===0) {
    // 	console.log('firefox');
    // 	$( window).resize( function(e) {
    // 		if ($(window).width() > 760) {
    // 			resizeShortText();
    // 		}
    // 	});
    // }

};





/**
 * (Static) class ExternalApps in namespace Utils provides methods
 * for interoperability with external apps (sharing, sending data, ...)
 * @class ExternalApps
 * @namespace Utils
 * @type {ExternalApps}
 */
Utils.ExternalApps = Utils.ExternalApps || {};


Utils.ExternalApps.pocketPopup = function(e) {
    try {
        console.log(Log.info('Pocket ...'));
        var i = e.target.getAttribute("data-title") || e.target.parentNode.getAttribute("data-title") || "";
        var b = e.target.getAttribute("data-url")   || e.target.parentNode.getAttribute("data-url")   || "";
        var t = e.target.getAttribute("data-tags")  || e.target.parentNode.getAttribute("data-tags")   || "";
        var f = 550,
            g = 320,
            j = screen.height,
            c = screen.width,
            l = Math.round((c / 2) - (f / 2)),
            d = 0;
        if (j > g) {
            d = Math.round((j / 2) - (g / 2))
        }
        var k = window.open("https://getpocket.com/save?url=" + encodeURIComponent(b) + "&title=" + encodeURIComponent(i) + "&tags=" + encodeURIComponent(t) + "&_=" + new Date().getTime(), "", "left=" + l + ",top=" + d + ",width=550,height=320,personalbar=0,toolbar=0,scrollbars=1,resizable=1");
        if (k == null) {
            return;
        }
        k.focus();
    } catch (h) {
        console.warn(Log.err('pocketPopup failed'));
    }
};


/**
 *  Pocket bookmarklet script (http://getpocket.com/add/?ep=1)
 *  Shows Pocket strip at the top of the page.
 *   - with custom improvements for SARR by xinad
 */
Utils.ExternalApps.pocketPopupBetter = function(evt) {

    var url   = evt.target.getAttribute("data-url")   || evt.target.parentNode.getAttribute("data-url")   || "";
    var title = evt.target.getAttribute("data-title") || evt.target.parentNode.getAttribute("data-title") || "";

    var e = function(t, n, r, i, s) {
        var o=[1629107,4850219,5856839,5913163,3048228,1995239,4349442,1115900,2235251,4458653];
        var i=i||0,u=0,n=n||[],r=r||0,s=s||0;var a={'a':97,'b':98,'c':99,'d':100,'e':101,'f':102,'g':103,'h':104,'i':105,'j':106,'k':107,'l':108,'m':109,'n':110,'o':111,'p':112,'q':113,'r':114,'s':115,'t':116,'u':117,'v':118,'w':119,'x':120,'y':121,'z':122,'A':65,'B':66,'C':67,'D':68,'E':69,'F':70,'G':71,'H':72,'I':73,'J':74,'K':75,'L':76,'M':77,'N':78,'O':79,'P':80,'Q':81,'R':82,'S':83,'T':84,'U':85,'V':86,'W':87,'X':88,'Y':89,'Z':90,'0':48,'1':49,'2':50,'3':51,'4':52,'5':53,'6':54,'7':55,'8':56,'9':57,'\/':47,':':58,'?':63,'=':61,'-':45,'_':95,'&':38,'$':36,'!':33,'.':46};
        if(!s||s==0){t=o[0]+t}for(var f=0;f<t.length;f++){var l=function(e,t){return a[e[t]]?a[e[t]]:e.charCodeAt(t)}(t,f);if(!l*1)l=3;var c=l*(o[i]+l*o[u%o.length]);n[r]=(n[r]?n[r]+c:c)+s+u;var p=c%(50*1);if(n[p]){var d=n[r];n[r]=n[p];n[p]=d}u+=c;r=r==50?0:r+1;i=i==o.length-1?0:i+1}if(s==157){var v='';for(var f=0;f<n.length;f++){v+=String.fromCharCode(n[f]%(25*1)+97)}o=function(){};return v+'ec55ba4265'}else{return e(u+'',n,r,i,s+1)}
    };

    var t = document,
        n = url,
        r = title;
    var i = e(n);
    var s = t.createElement('script');
    s.type = 'text/javascript';
    s.src = 'https://getpocket.com/b/r4.js?h=' + i + '&u=' + encodeURIComponent(n) + '&t=' + encodeURIComponent(r);
    e = i = function () {};
    var o = t.getElementsByTagName('head')[0] || t.documentElement;
    o.appendChild(s);


    // automatic hiding pocket popup (after 3 sec), when user isn't logged

    (function autoHidePocketPopup() {
    	setTimeout(function() {
            	console.log(Log.info('removing Pocket bar'));
            	if ($('#PKT_BM_OVERLAY').text().search('Please login.') > 0 )
            		$('#PKT_BM_OVERLAY').slideUp(700, function() {this.remove();});

				$("head > script[src^='https://getpocket.com/b/r4.js'").each(function(){
					this.remove();
				});
            }, 3000);
    })();

};





/*
    Skryvanie address baru na mobile
 */
if (/iPhone/i.test(navigator.userAgent)) {
    window.addEventListener("load", function () {
        setTimeout(function () {
            window.scrollTo(0, 1); // Hide the address bar!
        }, 0);
    });
}



/*
 ADD TO POCKET:

 https://getpocket.com/edit
 ?url=http%3A%2F%2Fvtm.e15.cz%2Fvitaminove-doplnky-mohou-nadelat-vic-skody-nez-uzitku
 &title=Vitam%C3%ADnov%C3%A9+dopl%C5%88ky+mohou+nad%C4%9Blat+v%C3%ADc+%C5%A1kody+ne%C5%BE+u%C5%BEitku
 */

/*
 ADD TO POCKET:

 https://getpocket.com/edit?url=http%3A%2F%2Fvtm.e15.cz&title=Title&tags=tag
 */
