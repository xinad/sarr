/**
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */
package models.unit;

import models.entity.Article;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import java.util.List;

import static org.junit.Assert.*;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;


public class ArticleTest extends WithApplication {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
        new Article("Some title", "http://some.url").save();
        new Article("Some another title", "http://some.another.url").save();
    }

    @Test
    public void retrieveArticleByID() {
        Article article = Article.find.byId(1L);
        assertNotNull(article);
        assertEquals("Some title", article.title);
        assertEquals("http://some.url", article.link);
    }

    @Test
    public void retrieveArticles() {
        List<Article> articles = Article.find.all();
        assertNotNull(articles);
        assertFalse(articles.size() == 0);
        assertEquals(articles.size(), 2);

        for (Article article : articles) {
            assertNotNull(article.id);
            assertTrue(article.title.contains("title"));
            assertTrue(article.link.contains("http://"));
        }
    }

}
