/**
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */
package models.unit;


import models.entity.User;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;

/**
 * Created with IntelliJ IDEA.
 * By: Filip Vozár
 * Date: 18.10.2013 | Time: 19:59
 */
public class UserTest extends WithApplication {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
        new User("bob@gmail.com", "Bob", "secret").save();
    }

    @Test
    public void retrieveUser() {
        User bob = User.find.where().eq("email", "bob@gmail.com").findUnique();
        assertNotNull(bob);
        assertEquals("Bob", bob.nick);
        assertEquals("secret", bob.password);
    }
}
