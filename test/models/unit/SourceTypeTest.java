/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */
package models.unit;

import models.entity.SourceType;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;

public class SourceTypeTest extends WithApplication {
    @Before
    public void setUp() throws Exception {
        start(fakeApplication(inMemoryDatabase()));
        new SourceType("RSS").save();
        new SourceType("Atom").save();
        new SourceType("Custom").save();
    }

    @Test
    public void retrieveSourceTypeById() {
        SourceType sourceType = SourceType.find.byId(2L);

        assertNotNull(sourceType);
        assertEquals("Atom", sourceType.title);
    }

    @Test
    public void retrieveSourceTypes() {
        List<SourceType> sourceTypes = SourceType.find.all();

        assertNotNull(sourceTypes);
        assertTrue(sourceTypes.size() == 3);

        for (SourceType sType : sourceTypes) {
            assertNotNull(sType.id);
            assertNotNull(sType.title);
        }
    }

}
