/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.unit;

import models.entity.Keyword;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import java.util.List;

import static org.junit.Assert.*;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;


public class KeywordsTest extends WithApplication {
    @Before
    public void setUp() throws Exception {
        start(fakeApplication(inMemoryDatabase()));
        Keyword keyword = new Keyword();
        keyword.content = "my, new, keyword";
        keyword.save();

        keyword = new Keyword();
        keyword.content = "java, scala, play framework, whatever";
        keyword.save();
    }

    @Test
    public void retrieveKeywordById() {
        Keyword keyword = Keyword.find.byId(1L);

        assertNotNull(keyword);
        assertFalse(keyword.content.length() == 0);
        assertEquals("my, new, keyword", keyword.content);
    }

    @Test
    public void retrieveKeywords() {
        List<Keyword> keywords = Keyword.find.all();

        assertNotNull(keywords);
        assertFalse(keywords.size() == 0);
        assertNotSame(keywords.get(0), keywords.get(1));

        for (Keyword keyword : keywords) {
            assertNotNull(keyword);
            assertTrue(keyword.content.contains(","));
        }
    }
}
