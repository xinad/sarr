/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.unit;

import models.entity.Tag;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;


public class TagTest extends WithApplication {

    @Before
    public void setUp() throws Exception {
        start(fakeApplication(inMemoryDatabase()));
        Tag tag = new Tag();
        tag.title = "Java";
        tag.save();

        tag = new Tag();
        tag.title = "Play framework";
        tag.save();

        tag = new Tag();
        tag.title = "hash";
        tag.save();
    }

    @Test
    public void retrieveTagById() {

    }
}
