/**
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */
package models.unit;

import models.entity.Source;
import models.entity.SourceType;
import org.junit.Before;
import org.junit.Test;
import play.test.WithApplication;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;

public class SourceTest extends WithApplication {

    @Before
    public void setUp() throws Exception {
        start(fakeApplication(inMemoryDatabase()));
        new SourceType("title").save();
        Source source = new Source();
        source.title = "Test title";
        source.description = "This is fake description of fake source";
        source.link = "http://some.fake.link";
        source.type = SourceType.find.byId(1L);
        source.save();

        source = new Source();
        source.title = "This title makes the source titled";
        source.publishDate = new Date();
        source.description = "Description is important! Anytime and anywhere!";
        source.link = "http://fake.link";
        source.type = SourceType.find.byId(1L);
        source.save();
    }

    @Test
    public void retrieveSourceById() {
        Source source = Source.find.byId(1L);
        assertNotNull(source);
        assertEquals("Test title", source.title);
        assertTrue(source.description.contains("is fake"));
    }

    @Test
    public void retrieveSources() {
        List<Source> sources = Source.find.all();

        assertNotNull(sources);
        assertEquals(sources.size(), 2);
        assertNotSame(sources.get(0), sources.get(1));

        for (Source source : sources) {
            assertNotNull(source.id);
            assertTrue(source.title.contains("title"));
            assertTrue(source.link.length() != 0);
        }
    }
}
