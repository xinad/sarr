package models.dataAccessObject.ebean;

import com.fasterxml.jackson.databind.JsonNode;
import models.dataAccessObject.IArticleDao;
import models.entity.Article;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.libs.Json;
import play.test.FakeApplication;
import play.test.Helpers;
import services.UserArticleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleDaoTest {

    private FakeApplication app;
    private JndiDatabaseTester databaseTester;

    private IArticleDao articleDao;
    private UserDao userDao;
    private UserArticleService userArticleService;
    private UserArticleDao userArticleDao;
    private UserArticleTagDao userArticleTagDao;


    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");
        settings.put("application.global", "Global");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        articleDao = new ArticleDao();
        userDao = new UserDao();
        userArticleDao = new UserArticleDao();
        userArticleTagDao = new UserArticleTagDao();
        userArticleService = new UserArticleService(userDao, articleDao, userArticleDao, userArticleTagDao);
    }

    @Test
    public void testFindAll() throws Exception {

        List<Article> articles = articleDao.findAll();

        for (Article article : articles) {
            System.out.println(article.id);
            System.out.println(article.author);
            System.out.println(article.title);
            System.out.println(article.description);
            System.out.println(article.link);
            System.out.println(article.publishDate);
            System.out.println(article.source.link);

            Assert.assertNotNull(article);
            Assert.assertNotNull(article.id);
        }
    }

    @Test
    public void testFindById() throws Exception {
        Article article = articleDao.findById(1L);
        System.out.println(article.id);
        System.out.println(article.author);
        System.out.println(article.title);
        System.out.println(article.description);
        System.out.println(article.link);
        System.out.println(article.publishDate);
        System.out.println(article.source.link);

        Assert.assertNotNull(article);
        Assert.assertNotNull(article.id);
    }

    @Test
    public void findArticlesByUser() throws Exception {
//        JsonNode articles = userArticleService.findArticlesForUserWithId(1L);
//
//
//        for (Article article : articles) {
//            System.out.println("article.id = " + article.id);
//            System.out.println("article = " + article.author);
//            System.out.println("article.title = " + article.title);
//            System.out.println("article.description = " + article.description);
//
//            Assert.assertNotNull(article.id);
//        }

    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }

}
