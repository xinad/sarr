/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.dataAccessObject.ebean;

import models.dataAccessObject.ebean.UserDao;
import models.entity.Article;
import models.entity.User;
import models.entity.UserArticle;
import models.entity.UserSource;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDaoTest {


    private FakeApplication app;
    private JndiDatabaseTester databaseTester;

    private UserDao userDao;

    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr_test");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        userDao = new UserDao();
    }

    @Test
    public void testFindAll() throws Exception {
        List<User> users = this.userDao.findAll();

        for (User user : users) {
            System.out.println("user.id = " + user.id);
            System.out.println("user.firstName + user.lastName = " + user.firstName + " " + user.lastName);
            for (UserSource userSource : user.userSources) {
                System.out.println("userSource.source.id = " + userSource.source.id);
                System.out.println("userSource.source.title = " + userSource.source.title);
                System.out.println("userSource.source.description = " + userSource.source.description);
            }

            for (UserArticle userArticle : user.userArticles) {
                System.out.println("userArticle.article.id = " + userArticle.article.id);
                System.out.println("userArticle.article.author = " + userArticle.article.author);
                System.out.println("userArticle.article.title = " + userArticle.article.title);
            }

        }

    }

    @Test
    public void testFindById() throws Exception {

    }

//    @Test
//    public void testFindArticlesForUser() throws Exception {
//        Set<UserArticle> articlesForUser = userDao.findArticlesForUser(1L);

//        for (UserArticle userArticles : articlesForUser) {
//            System.out.println("article.article.id = " + userArticles.article.id);
//            System.out.println("article.article.title = " + userArticles.article.title);
//            System.out.println("article.article.author = " + userArticles.article.author);
//
//            assertNotNull(userArticles.article.id);
//        }

//    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }
}
