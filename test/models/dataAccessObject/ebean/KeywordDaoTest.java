package models.dataAccessObject.ebean;

import models.entity.Keyword;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class KeywordDaoTest {

    private FakeApplication app;
    private JndiDatabaseTester databaseTester;

    private KeywordDao keywordDao;

    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr_test");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");
        settings.put("application.global", "Global");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        keywordDao = new KeywordDao();
    }

    @Test
    public void testFindAll() throws Exception {
        List<Keyword> keywords = keywordDao.findAll();

        for (Keyword keyword : keywords) {
            Assert.assertNotNull(keyword.id);
            Assert.assertNotNull(keyword.content);
            Assert.assertNotNull(keyword.user);

            System.out.println("keyword.id = " + keyword.id);
            System.out.println("keyword.content = " + keyword.content);
            System.out.println("keyword.user.firstName + keyword.user.lastName = " + keyword.user.firstName + " " + keyword.user.lastName);
        }

    }

    @Test
    public void testFindById() throws Exception {
        Keyword keyword = this.keywordDao.findById(1L);

        Assert.assertNotNull(keyword.id);
        Assert.assertNotNull(keyword.content);
        Assert.assertNotNull(keyword.user);

        System.out.println("keyword.id = " + keyword.id);
        System.out.println("keyword.content = " + keyword.content);
        System.out.println("keyword.user.firstName + keyword.user.lastName = " + keyword.user.firstName + " " + keyword.user.lastName);
    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }

}
