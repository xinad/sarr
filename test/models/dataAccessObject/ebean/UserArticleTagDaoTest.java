package models.dataAccessObject.ebean;

import models.dataAccessObject.IArticleDao;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;
import services.UserArticleService;

import java.util.HashMap;
import java.util.Map;


public class UserArticleTagDaoTest {

    private FakeApplication app;
    private JndiDatabaseTester databaseTester;

    private IArticleDao articleDao;
    private UserDao userDao;
    private UserArticleService userArticleService;
    private UserArticleDao userArticleDao;
    private UserArticleTagDao userArticleTagDao;


    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");

        settings.put("application.global", "Global");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        articleDao = new ArticleDao();
        userDao = new UserDao();
        userArticleDao = new UserArticleDao();
        userArticleTagDao = new UserArticleTagDao();
        userArticleService = new UserArticleService(userDao, articleDao, userArticleDao, userArticleTagDao);
    }

    @Test
    public void testSaveTag() throws Exception {
        userArticleService.setTagsToArticleForUser(2L, 1L, "[ \"Amsterdam\",\n" +
                "  \"London\",\n" +
                "  \"Paris\",\n" +
                "  \"Washington\",\n" +
                "  \"New York\",\n" +
                "  \"Los Angeles\",\n" +
                "  \"Sydney\",\n" +
                "  \"Melbourne\",\n" +
                "  \"Canberra\",\n" +
                "  \"Beijing\",\n" +
                "  \"New Delhi\",\n" +
                "  \"Kathmandu\",\n" +
                "  \"Cairo\",\n" +
                "  \"Cape Town\",\n" +
                "  \"Kinshasa\"\n" +
                "]");

        userArticleService.setTagsToArticleForUser(2L, 1L, "[ \"Amsterdam\", \"London\", \"Paris\" ]");
    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }
}
