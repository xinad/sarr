package models.dataAccessObject.ebean;

import models.dataAccessObject.ebean.SourceDao;
import models.entity.Article;
import models.entity.Source;
import models.entity.UserSource;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SourceDaoTest {

    private FakeApplication app;
    private JndiDatabaseTester databaseTester;

    private SourceDao sourceDao;

    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr_test");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");
        settings.put("application.global", "Global");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        sourceDao = new SourceDao();
    }

    @Test
    public void testFindAll() throws Exception {

        List<Source> sources = sourceDao.findAll();

        for (Source source : sources) {
            System.out.println("source.id = " + source.id);
            System.out.println("source.title = " + source.title);
            System.out.println("source.description = " + source.description);
            System.out.println("source.link = " + source.link);
            System.out.println("source.publishDate = " + source.publishDate);
            System.out.println("source.type.title = " + source.type.title);
            for (Article article : source.articles) {
                System.out.println("article.id = " + article.id);
                System.out.println("article.author = " + article.author);
                System.out.println("article.title = " + article.title);
            }
            for (UserSource userSource : source.userSources) {
                System.out.println("userSource.user.firstName + userSource.user.lastName = " + userSource.user.firstName + " " + userSource.user.lastName);
            }

            Assert.assertNotNull(source);
            Assert.assertNotNull(source.id);
        }
    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }

}
