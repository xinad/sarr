/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.integration;

import com.avaje.ebean.Ebean;
import models.entity.User;
import models.entity.UserSource;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class UserSourceIntegrationTest {

    FakeApplication app;
    JndiDatabaseTester databaseTester;

    @Before
    public void startApp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr_test");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.*");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();
    }

    @Test
    public void databaseConnectionTest() {
        try {
            IDataSet dataSet = databaseTester.getConnection().createDataSet();
            ITable article = dataSet.getTable("Article");

            assertFalse(article.getRowCount() == 0);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUserSources() {

        List<User> users = Ebean.find(User.class).findList();
        for (User user : users) {
            System.out.println("USER: " + user.firstName + " " + user.lastName);
            System.out.println("HAS THIS SOURCES: ");

            List<UserSource> sources = Ebean.find(UserSource.class).fetch("source").where().eq("user.id", user.id).findList();
            for (UserSource source : sources) {
                System.out.println("\t" + source.source.title);
            }
        }
    }

    @After
    public void stopApp() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }
}
