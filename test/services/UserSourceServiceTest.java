package services;

import models.dataAccessObject.ebean.SourceDao;
import models.dataAccessObject.ebean.UserDao;
import models.dataAccessObject.ebean.UserSourceDao;
import models.entity.Source;
import models.entity.User;
import models.entity.UserSource;
import org.dbunit.JndiDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;
import play.test.FakeApplication;
import play.test.Helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class UserSourceServiceTest {


    private FakeApplication app;
    private JndiDatabaseTester databaseTester;
    private UserSourceService userSourceService;
    private UserDao userDao;
    private SourceDao sourceDao;
    private UserSourceDao userSourceDao;

    @Before
    public void setUp() throws Exception {
        Map<String, String> settings = new HashMap<>();
        settings.put("db.default.url", "jdbc:mariadb://localhost/sarr");
        settings.put("db.default.driver", "org.mariadb.jdbc.Driver");
        settings.put("db.default.user", "sarr_user");
        settings.put("db.default.password", "sarr_password");
        settings.put("db.default.jndiName", "DefaultDS");
        settings.put("ebean.default", "models.entity.*");
        settings.put("application.global", "Global");

        app = Helpers.fakeApplication(settings);
        Helpers.start(app);

        databaseTester = new JndiDatabaseTester("DefaultDS");
        IDataSet initialDataSet = new FlatXmlDataSetBuilder().build(play.Play.application().getFile("/test/models/dataset.xml"));
        databaseTester.setDataSet(initialDataSet);
        databaseTester.onSetup();

        userDao = new UserDao();
        sourceDao = new SourceDao();
        userSourceDao = new UserSourceDao();
        userSourceService = new UserSourceService(userDao, sourceDao, userSourceDao);
    }

    @After
    public void tearDown() throws Exception {
        databaseTester.onTearDown();
        Helpers.stop(app);
    }

    @Test
    public void testFindAll() throws Exception {
        List<Source> all = this.userSourceService.findAll();

        Assert.notNull(all);
        Assert.notEmpty(all);

        for (Source source : all) {
            Assert.notNull(source.id);
        }
    }

    @Test
    public void testFindById() throws Exception {
        Source source = this.userSourceService.findById(1L);

        Assert.notNull(source);
        Assert.notNull(source.id);
    }

    @Test
    public void testFindByLink() throws Exception {
        Source source = new Source();
        source.link = "http://www.google.sk";
        source.save();

        Source sourceByLink = this.userSourceService.findByLink("http://www.google.sk");
        Source badLink = this.userSourceService.findByLink("thisIsBadURL");

        Assert.notNull(sourceByLink);
        Assert.isTrue(source.id.equals(sourceByLink.id));

        Assert.isNull(badLink);
    }

    @Test
    public void testFindByTitle() throws Exception {
        List<Source> sources = this.userSourceService.findByTitle("a");

        Assert.notNull(sources);
        Assert.notEmpty(sources);

        for (Source source : sources) {
            Assert.notNull(source.id);
        }

    }

    @Test
    public void testAddSource() throws Exception {
        String url = "http://some.awesome.url";
        Long newSourceId = this.userSourceService.addSource(url);

        Source source = this.userSourceService.findById(newSourceId);

        Assert.isTrue(source.link.equals(url));
    }

    @Test
    public void testAddSourceToUser() throws Exception {
        Source source = new Source();
        source.link = "http://some.url";
        source.save();

        this.userSourceService.addSourceToUser(source.id, 2L);

        boolean found = false;
        User user = userDao.findById(2L);
        for (UserSource userSource : user.userSources) {
            if (userSource.source.id.equals(source.id))
                found = true;
        }

        Assert.isTrue(found);
    }

    @Test
    public void testDropSource() throws Exception {
        Source source = new Source();
        source.link = "http://some.nice.url/nicer/post";
        source.save();

        Long id = source.id;
        Assert.notNull(id);

        this.userSourceService.dropSource(id);

        source = sourceDao.findById(id);

        Assert.isTrue(source == null);
    }

    @Test
    public void testDropSourceFromUser() throws Exception {
        this.userSourceService.dropSourceFromUser(1L, 1L);

        User user = userDao.findById(1L);
        Set<UserSource> userSources = user.userSources;

        Assert.notEmpty(userSources);
        Assert.isTrue(userSources.size() == 1);


        for (UserSource userSource : userSources) {
            Assert.isTrue(userSource.source.id != 1L);
        }
    }
}
