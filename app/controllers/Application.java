package controllers;

import play.Routes;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import services.backgroundServices.NewsCheckerService;
import temporary.User;
import views.html.index;
import views.html.login;
import views.html.showUser;

import static play.data.Form.form;

public class Application extends Controller {

    /**
     * Home page. route: /
     */
    public Result index() {
        return ok(
                index.render("Home")
        );
    }

    /**
     * Login/Create Account page. route: /login
     */
    public Result loginScreen() {
        return ok(
                login.render("Login & Sign Up", form(Login.class))    //null is user
        );
    }

    /**
     * // -- Authentication
     */
    public static class Login {

        public String email;
        public String password;

        public String validate() {
            if (User.authenticate(email, password) == null) {
                return Messages.get("login.alert.invalidUserOrPassword");
            }
            return null;
        }
    }

    /**
     * Logout and clean the session.
     */
    public static Result logout() {
        session().clear();
        flash("success", Messages.get("login.alert.loggedOut"));
        return redirect(
                routes.Application.loginScreen()
        );
    }

    /**
     * Handle login form submission.
     */
    public Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render("Login & Sign Up", loginForm));
        } else {
            session().clear();
            session("email", loginForm.get().email);
            flash("success", Messages.get("login.alert.loggedIn"));
            return redirect(
                    routes.Application.index()
            );
        }
    }

    //@SecureSocial.SecuredAction
    @SecureSocial.UserAwareAction
    public static Result showUser() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }
        return ok(showUser.render(user));
    }


    public static Result javascriptRoutes() {
        response().setContentType("text/javascript");
        return ok(
                Routes.javascriptRouter("jsRoutes",
                        routes.javascript.Articles.listNotRead(),
                        routes.javascript.Articles.listStarred(),
                        routes.javascript.Articles.listAsJSON(),
                        routes.javascript.Articles.markRead(),
                        routes.javascript.Articles.addStar(),
                        routes.javascript.Articles.listWithTag(),
                        routes.javascript.Sources.addSource(),
                        routes.javascript.Tags.listAsJSON(),
                        routes.javascript.Tags.addTags()
                )
        );
    }

}
