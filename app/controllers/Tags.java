package controllers;

import com.google.inject.Inject;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import services.IUserArticleService;
import services.IUserAuthenticationService;

import java.util.Set;

import static play.data.Form.form;


public class Tags extends Controller {

    @Inject
    private IUserArticleService userArticleService;
    @Inject
    private IUserAuthenticationService userAuthenticationService;


    @SecureSocial.UserAwareAction
    public Result listAsJSON() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        Set<String> tags = userArticleService.findTagsForUser(userId);
        return ok(
                Json.toJson(tags)
        );
    }

    @SecureSocial.UserAwareAction
    public Result addTags(Long articleId) {
        DynamicForm dynamicForm = form().bindFromRequest();
        String tags = dynamicForm.get("tags");

        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        Long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        try {
            this.userArticleService.setTagsToArticleForUser(userId, articleId, tags);
            return ok("Article with id " + articleId + " has tags " + tags);
        } catch (IllegalArgumentException e) {
            return notFound(e.getMessage());
        }
    }


}
