package controllers;


import com.google.inject.Inject;
import models.entity.Source;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import services.IUserAuthenticationService;
import services.IUserSourceService;
import services.backgroundServices.IArticleParser;

import static play.data.Form.form;

public class Sources extends Controller {

    @Inject
    private IUserAuthenticationService userAuthenticationService;

    @Inject
    private IUserSourceService userSourceService;

    @Inject
    private IArticleParser articleParser;

    @SecureSocial.UserAwareAction
    public Result addSource() {
        DynamicForm dynamicForm = form().bindFromRequest();
        String url = dynamicForm.get("url");

        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        Long userId = this.userAuthenticationService.findUserByEmail(user.email().get()).id;

        Source source = this.articleParser.buildSourceFromUrl(url);
        this.userSourceService.addSourceToUser(source.id, userId);

        return ok("Source with id " + source.id + " was added to user with id " + userId);
    }
}
