package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;
import models.entity.Article;
import models.entity.UserSourceGroup;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import services.IUserArticleService;
import services.IUserAuthenticationService;
import views.html.articles;

import java.util.List;
import java.util.Set;

import static play.data.Form.form;

/**
 *
 */
public class Articles extends Controller {

    @Inject
    private IUserArticleService userArticleService;
    @Inject
    private IUserAuthenticationService userAuthenticationService;

    @SecureSocial.UserAwareAction
    public Result listAsJSON() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;  //id pouzivatela z DB pre email s ktorym sa prihlasil

        JsonNode articles = userArticleService.findArticlesForUserWithId(userId);
        return ok(
                articles
        );
    }

    @SecureSocial.UserAwareAction
    public Result list() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;      //id pouzivatela z DB pre email s ktorym sa prihlasil

        Set<UserSourceGroup> sourcesGroup = userArticleService.getGroupsById(userId);
        return ok(
                articles.render(sourcesGroup, user)
        );
    }

    @SecureSocial.UserAwareAction
    public Result listNotRead() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        JsonNode unreadArticles = userArticleService.findUnreadArticlesForUser(userId);

        return ok(unreadArticles);
    }

    @SecureSocial.UserAwareAction
    public Result listStarred() {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        JsonNode unreadArticles = userArticleService.findStarredArticlesForUser(userId);

        return ok(unreadArticles);
    }

    @SecureSocial.UserAwareAction
    public Result markRead(Long articleId) {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        Long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        try {
            this.userArticleService.markArticleAsReadForUser(userId, articleId);
            return ok("Article with id " + articleId + " updated to seen for user with id " + userId);
        } catch (IllegalArgumentException e) {
            return notFound(e.getMessage());
        }
    }

    @SecureSocial.UserAwareAction
    public Result addStar(Long articleId, Boolean isStar) {
        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        Long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        try {
            if (isStar) {
                this.userArticleService.addStarToArticleForUser(userId, articleId);
                return ok("Article with id " + articleId + " is now starred for user with id " + userId);
            } else {
                this.userArticleService.removeStarToArticleForUser(userId, articleId);
                return ok("Article with id " + articleId + " is now unstarred for user with id " + userId);
            }
        } catch (IllegalArgumentException e) {
            return notFound(e.getMessage());
        }
    }

    @SecureSocial.UserAwareAction
    public Result listWithTag() {
        String tag = form().bindFromRequest().get("tag");

        Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
        if (user == null) {
            return redirect(
                    controllers.routes.Application.index()
            );
        }

        Long userId = userAuthenticationService.findUserByEmail(user.email().get()).id;

        Long tagId = userArticleService.getTagIdForUser(userId, tag);
        if (tagId != -1) {
            JsonNode articlesWithTagForUser = userArticleService.findArticlesWithTagForUser(userId, tagId);
            return ok(articlesWithTagForUser);
        } else {
            return notFound("There aren't any articles with given tag");
        }
    }
}
