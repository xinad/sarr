package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.entity.Article;
import models.entity.UserArticleTag;
import models.entity.UserSourceGroup;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: filip
 * Date: 12/1/13
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IUserArticleService {

    public JsonNode findArticlesForUserWithId(Long id);

    public Set<String> findTagsForUser(Long userId);

    public JsonNode findArticlesWithTagForUser(Long userId, Long tagId);

    public Set<UserSourceGroup> getGroupsById(Long id);

    public Article findArticleForUser(Long userId, Long articleId);

    public void markArticleAsReadForUser(Long userId, Long articleId) throws IllegalArgumentException;

    public void addStarToArticleForUser(Long userId, Long articleId) throws IllegalArgumentException;

    public void removeStarToArticleForUser(Long userId, Long articleId) throws IllegalArgumentException;
   
    public void setTagsToArticleForUser(Long userId, Long articleId, String tags) throws IllegalArgumentException;

    public JsonNode findUnreadArticlesForUser(Long userId);

    public JsonNode findStarredArticlesForUser(Long userId);

    public Long getTagIdForUser(Long userId, String tag);

    public void addArticleToUser(Long userId, Long id);
}
