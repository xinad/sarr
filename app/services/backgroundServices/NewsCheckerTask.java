/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package services.backgroundServices;


import models.entity.Article;
import models.entity.Source;
import models.entity.UserSource;
import play.Logger;
import services.IUserArticleService;
import services.IUserSourceService;

import java.net.MalformedURLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 1.12.2013 | Time: 20:20
 */
public class NewsCheckerTask implements Runnable {

    private IArticleParser articleParser;
    private final Logger.ALogger LOG = Logger.of("backgroundService");
    private NewArticleBinder articleBinder;


    public NewsCheckerTask(IArticleParser articleParser) {
        this.articleParser = articleParser;
        articleBinder = new NewArticleBinder();
    }

    @Override
    public void run() {
        /*
         TODO:
          * get List<Source> from database
          * for each source find new articles
                * parsing process
          * add new articles into database
        */

        // get List<Source> from database:
        List<Source> allSources = Source.find.all();


        // for each source find new articles
        for (Source source : allSources) {
            // add new articles into database
            saveNewArticlesFromSource(source);
        }
    }

    private void saveNewArticlesFromSource(Source source) {
        List<Article> freshArticlesFromFeed = getAllArticlesFromInternet(source);

        for (Article article : freshArticlesFromFeed) {
            try {
                article.save();
            } catch (Exception e) {
                //empty
                LOG.debug("SARR > Vkladam nevlozitelne :D");
            }
        }

        source.articles = Article.find.where().eq("source.id", source.id).findSet();
        source.save();

        for (UserSource userSource : source.userSources) {
            articleBinder.bindArticleToUser(userSource.user.id);
        }
    }

    private List<Article> getAllArticlesFromInternet(Source source) {
        List<Article> allArticles = null;
        try {
            allArticles = articleParser.getArticlesFromSource(source);

        } catch (MalformedURLException e) {

            LOG.error("SARR > ArticleParser.getArticlesFromSource(): Source id: {} ({}) has bad URL: \"{}\" (MalformedUrlException)",
                    source.id, source.title, source.link);
            e.printStackTrace();
        }

        return allArticles;
    }
}
