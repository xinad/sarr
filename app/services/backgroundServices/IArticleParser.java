/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package services.backgroundServices;

import models.entity.Article;
import models.entity.Source;

import java.net.MalformedURLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 1.12.2013 | Time: 22:49
 */
public interface IArticleParser {


    List<Article> getArticlesFromSource(Source source) throws MalformedURLException;

    Source buildSourceFromUrl(String url);
}
