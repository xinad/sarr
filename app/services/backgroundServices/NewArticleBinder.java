package services.backgroundServices;


import com.google.inject.Inject;
import models.dataAccessObject.ebean.*;
import models.entity.Article;
import models.entity.Source;
import services.IUserArticleService;
import services.IUserSourceService;
import services.UserArticleService;
import services.UserSourceService;

import javax.persistence.PersistenceException;
import java.util.List;

public class NewArticleBinder implements INewArticleBinder {

    @Inject
    public IUserArticleService userArticleService;

    @Inject
    public IUserSourceService userSourceService;

    public NewArticleBinder() {
        UserDao userDao = new UserDao();
        userArticleService = new UserArticleService(userDao, new ArticleDao(), new UserArticleDao(), new UserArticleTagDao());
        userSourceService = new UserSourceService(userDao, new SourceDao(), new UserSourceDao());
    }

    public void bindArticleToUser(Long userId) {

        List<Source> sources = userSourceService.findAllSourcesForUser(userId);
        for (Source source : sources) {
            for (Article article : source.articles) {
                try {
                    userArticleService.addArticleToUser(userId, article.id);
                } catch (PersistenceException e) {
                    //empty
                }
            }
        }
    }

}
