package services.backgroundServices;


public interface INewArticleBinder {

    void bindArticleToUser(Long userId);
}
