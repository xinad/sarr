/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package services.backgroundServices;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import models.entity.Article;
import models.entity.Source;
import models.entity.SourceType;
import play.Logger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 1.12.2013 | Time: 22:49
 */
public class ArticleParser implements IArticleParser {

    private List<Article> allArticlesFromXML;
    private final Logger.ALogger LOG = Logger.of("backgroundService");

    @Override
    public List<Article> getArticlesFromSource(Source source) throws MalformedURLException {

        allArticlesFromXML = new ArrayList<>();

        // 1. download RSS/atom dokument from link
        try (XmlReader reader = new XmlReader(new URL(source.uri))) {

            // 2. parse allArticlesFromXML from document
            SyndFeed feed = new SyndFeedInput().build(reader);


            for (Object o : feed.getEntries()) {
                SyndEntry entry = (SyndEntry) o;

                // prepare article object
                Article article = new Article(null, null);

                article.title = entry.getTitle();
                article.link = entry.getLink();
                article.description = entry.getDescription().getValue();
                article.publishDate = entry.getPublishedDate();
                article.source = source;
                article.author = entry.getAuthor();
                article.userArticles = null; // TODO article by nemalo mat referenciu na userArticles. Iba user by mal mat referenciu na allArticlesFromXML

                allArticlesFromXML.add(article);
            }

        } catch (Throwable e) {
            LOG.error("SARR > ArticleParser > getArticlesFromSource(): Error with parsing XML");
            e.printStackTrace();
        }

        return allArticlesFromXML;
    }


    public Source buildSourceFromUrl(String url) {
        Source source = Source.find.where().eq("uri", url).findUnique();

        if (source == null) {
            try (XmlReader reader = new XmlReader(new URL(url))) {

                // 2. parse allArticlesFromXML from document
                SyndFeed feed = new SyndFeedInput().build(reader);
                SourceType sourceType = SourceType.find.where().ilike("title", feed.getFeedType()).findUnique();
                if (sourceType == null) {
                    sourceType = new SourceType(feed.getFeedType());
                    sourceType.save();
                }

                String uri = feed.getUri();

                source = new Source();
                source.title = feed.getTitle();
                source.description = feed.getDescription();
                source.uri = (uri == null) ? url : uri;
                source.link = feed.getLink();
                source.publishDate = feed.getPublishedDate();
                source.type = sourceType;
                source.save();


            } catch (IOException | FeedException e) {
                LOG.error("SARR > ArticleParser > getArticlesFromSource(): Error with parsing XML");
                e.printStackTrace();
            }
        }

        return source;
    }

}
