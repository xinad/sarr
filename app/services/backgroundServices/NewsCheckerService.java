/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package services.backgroundServices;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class provides the latest data for news feeds in DB. <br/>
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 1.12.2013 | Time: 19:36
 */
public class NewsCheckerService {

    public IArticleParser parser = new ArticleParser();

    public void startParsing() {
        Timer timer = new Timer();
        final Runnable newsCheckerTask = new NewsCheckerTask(parser);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                newsCheckerTask.run();
            }
        }, 2000, 30000);
    }
}

