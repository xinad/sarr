/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package services.backgroundServices;

import models.entity.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Lukas Melega | Date: 1.12.2013 | Time: 22:31
 */
public class NewArticleFinder {


    public List<Article> filterNewArticles(List<Article> allArticles, Article lastArticlesFromDB) {
        if (lastArticlesFromDB == null) {
            return allArticles;
        }

        List<Article> filteredArticles = new ArrayList<>();

        for (Article article : allArticles) {

            if (!article.link.equals(lastArticlesFromDB.link)) {
                filteredArticles.add(article);
                break;
            }
        }

        return filteredArticles;
    }
}
