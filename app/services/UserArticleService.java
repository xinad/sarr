package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import models.dataAccessObject.IArticleDao;
import models.dataAccessObject.IUserArticleDao;
import models.dataAccessObject.IUserArticleTagDao;
import models.dataAccessObject.IUserDao;
import models.entity.*;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserArticleService implements IUserArticleService {

    private final IUserDao userDao;
    private final IArticleDao articleDao;
    private final IUserArticleDao userArticleDao;
    private final IUserArticleTagDao userArticleTagDao;

    @Inject
    public UserArticleService(IUserDao userDao, IArticleDao articleDao, IUserArticleDao userArticleDao, IUserArticleTagDao userArticleTagDao) {
        this.userDao = userDao;
        this.articleDao = articleDao;
        this.userArticleDao = userArticleDao;
        this.userArticleTagDao = userArticleTagDao;
    }

    @Override
    public JsonNode findArticlesForUserWithId(Long id) {
        User user = this.userDao.findById(id);
        List<Article> articles = this.articleDao.findArticlesByUser(user);
        List<UserArticle> articleStates = this.userArticleDao.getArticleStatesForUser(id);
        List<UserArticleTag> tagsToArticles = this.userArticleTagDao.findTagsToArticles(id);


        JsonNode jsonNode = Json.toJson(articles);
        if (jsonNode.isArray()) {
            int index = 0;
            ArrayNode json = (ArrayNode) jsonNode;

            Long articleId = null;
            for (JsonNode node : json) {
                ((ObjectNode) node).put("seen", articleStates.get(index).seen);
                ((ObjectNode) node).put("starred", articleStates.get(index).starred);

                articleId = ((ObjectNode) node).get("id").asLong();
                List<String> tagList = new ArrayList<>();
                for (UserArticleTag userArticleTag : tagsToArticles) {
                    if (userArticleTag.article.id.equals(articleId)) {
                        tagList.add(userArticleTag.tag.title);
                    }
                }

                JsonNode tags = Json.toJson(tagList);
                ((ObjectNode) node).put("tags", tags);

                index++;
            }
        }

        return jsonNode;
    }

    @Override
    public Set<String> findTagsForUser(Long userId) {
        return this.userArticleTagDao.findAllTagsForUser(userId);
    }

    @Override
    public Set<UserSourceGroup> getGroupsById(Long id) {
        return this.userDao.getGroupsById(id);
    }

    @Override
    public Article findArticleForUser(Long userId, Long articleId) {
        return this.articleDao.findArticleForUser(userId, articleId);
    }

    @Override
    public void markArticleAsReadForUser(Long userId, Long articleId) throws IllegalArgumentException {
        this.userArticleDao.markArticleAsReadForUser(userId, articleId);
    }

    @Override
    public void addStarToArticleForUser(Long userId, Long articleId) throws IllegalArgumentException {
        this.userArticleDao.setStarToArticleForUser(userId, articleId, true);
    }

    @Override
    public void removeStarToArticleForUser(Long userId, Long articleId) throws IllegalArgumentException {
        this.userArticleDao.setStarToArticleForUser(userId, articleId, false);
    }

    @Override
    public void setTagsToArticleForUser(Long userId, Long articleId, String tags) throws IllegalArgumentException {
        JsonNode json = Json.parse(tags);

        userArticleTagDao.removeAllTags(userId, articleId);

        if (json.isArray()) {
            for (JsonNode item : json) {
                userArticleTagDao.saveTag(userId, articleId, item.asText());
            }
        } else {
            throw new IllegalArgumentException("It isn't valid JSON file!");
        }
    }

    @Override
    public JsonNode findUnreadArticlesForUser(Long userId) {
        User user = this.userDao.findById(userId);
        List<Article> articles = this.userArticleDao.getUnReadArticles(userId);
        List<UserArticle> allArticleStates = this.userArticleDao.getArticleStatesForUser(userId);
        List<UserArticleTag> tagsToArticles = this.userArticleTagDao.findTagsToArticles(userId);

        List<UserArticle> articleStates = new ArrayList<>();
        for (UserArticle allArticleState : allArticleStates) {
            if (!allArticleState.seen)
                articleStates.add(allArticleState);
        }

        JsonNode jsonNode = Json.toJson(articles);
        if (jsonNode.isArray()) {
            int index = 0;
            ArrayNode json = (ArrayNode) jsonNode;

            Long articleId = null;
            for (JsonNode node : json) {
                ((ObjectNode) node).put("seen", false);
                ((ObjectNode) node).put("starred", articleStates.get(index).starred);

                articleId = node.get("id").asLong();
                List<String> tagList = new ArrayList<>();
                for (UserArticleTag userArticleTag : tagsToArticles) {
                    if (userArticleTag.article.id.equals(articleId)) {
                        tagList.add(userArticleTag.tag.title);
                    }
                }

                JsonNode tags = Json.toJson(tagList);
                ((ObjectNode) node).put("tags", tags);

                index++;
            }
        }

        return jsonNode;
    }

    @Override
    public JsonNode findStarredArticlesForUser(Long userId) {
        List<Article> articles = this.userArticleDao.getStarredArticles(userId);
        List<UserArticle> allArticleStates = this.userArticleDao.getArticleStatesForUser(userId);
        List<UserArticleTag> tagsToArticles = this.userArticleTagDao.findTagsToArticles(userId);

        List<UserArticle> articleStates = new ArrayList<>();
        for (UserArticle allArticleState : allArticleStates) {
            if (allArticleState.starred)
                articleStates.add(allArticleState);
        }

        JsonNode jsonNode = Json.toJson(articles);
        if (jsonNode.isArray()) {
            int index = 0;
            ArrayNode json = (ArrayNode) jsonNode;

            Long articleId = null;
            for (JsonNode node : json) {
                if (!articleStates.isEmpty()) {
                    ((ObjectNode) node).put("seen", articleStates.get(index).seen);
                }
                ((ObjectNode) node).put("starred", true);

                articleId = node.get("id").asLong();
                List<String> tagList = new ArrayList<>();
                for (UserArticleTag userArticleTag : tagsToArticles) {
                    if (userArticleTag.article.id.equals(articleId)) {
                        tagList.add(userArticleTag.tag.title);
                    }
                }

                JsonNode tags = Json.toJson(tagList);
                ((ObjectNode) node).put("tags", tags);

                index++;
            }
        }

        return jsonNode;
    }

    @Override
    public Long getTagIdForUser(Long userId, String tag) {
        Tag foundTag = this.userArticleTagDao.findTagForUser(userId, tag);

        return foundTag == null ? -1 : foundTag.id;
    }

    @Override
    public void addArticleToUser(Long userId, Long articleId) {
        User user = User.find.byId(userId);
        Article article = Article.find.byId(articleId);
        this.userArticleDao.addArticleToUser(user, article);
    }

    @Override
    public JsonNode findArticlesWithTagForUser(Long userId, Long tagId) {
        List<UserArticleTag> articlesWithTag = this.userArticleTagDao.findArticlesWithTag(userId, tagId);

        List<Article> articles = new ArrayList<>();
        List<Long> articleIDs = new ArrayList<>();
        for (UserArticleTag userArticleTag : articlesWithTag) {
            articles.add(userArticleTag.article);
            articleIDs.add(userArticleTag.article.id);
        }

        Map<Long, Map<String, Boolean>> stateForArticles = this.userArticleDao.getStateForArticles(userId, articleIDs);

        JsonNode jsonNode = Json.toJson(articles);
        if (jsonNode.isArray()) {
            ArrayNode json = (ArrayNode) jsonNode;

            for (JsonNode node : json) {
                Long id = node.get("id").asLong();

                ((ObjectNode) node).put("seen", stateForArticles.get(id).get("seen"));
                ((ObjectNode) node).put("starred", stateForArticles.get(id).get("starred"));


                List<String> tagList = new ArrayList<>();
                for (UserArticleTag userArticleTag : articlesWithTag) {
                    if (userArticleTag.article.id.equals(id)) {
                        tagList.add(userArticleTag.tag.title);
                    }
                }

                JsonNode tags = Json.toJson(tagList);
                ((ObjectNode) node).put("tags", tags);
            }

        }

        return jsonNode;
    }

}
