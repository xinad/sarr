package services;


import models.entity.Source;

import java.util.List;

/**
 *
 */
public interface IUserSourceService {

    /**
     * Find all sources and return them as list.
     *
     * @return List with all stored sources.
     */
    public List<Source> findAll();

    /**
     * Find one source with given identifier.
     *
     * @param id Id for source to find.
     * @return Source with given id.
     */
    public Source findById(Long id);

    /**
     * Find source according to its URL link.
     *
     * @param link URL link of the source.
     * @return Source with given link.
     */
    public Source findByLink(String link);

    /**
     * Find source or sources by its/their title. This method uses given title as substring for searching query.
     * <p/>
     * <br /><b>Example:</b>
     * <p/>
     * <p>You want to search sources with "blog" in theirs titles.<br />
     * The query will be as follows:
     * <pre>SELECT ... WHERE <b>title = %blog%;</b></pre></p>
     *
     * @param title Title to be searched.
     * @return List with sources with given part of title.
     */
    public List<Source> findByTitle(String title);


    public Long addSource(String url);

    public void addSourceToUser(Long sourceId, Long userId);

    public void dropSource(Long sourceId);

    public void dropSourceFromUser(Long sourceId, Long userId);

    List<Source> findAllSourcesForUser(Long userId);
}
