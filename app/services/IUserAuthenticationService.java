package services;

import models.entity.User;

/**
 * Created with IntelliJ IDEA.
 * User: Mitwoc
 * Date: 8.12.2013
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public interface IUserAuthenticationService {
    public User findUserByEmail(String email);
}
