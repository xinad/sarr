package services;

import com.google.inject.Inject;
import models.dataAccessObject.IUserDao;
import models.entity.User;

/**
 * Created with IntelliJ IDEA.
 * User: Mitwoc
 * Date: 8.12.2013
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
public class UserAuthenticationService implements IUserAuthenticationService {

    private final IUserDao userDao;

    @Inject
    public UserAuthenticationService(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User findUserByEmail(String email) {
        return userDao.findByEmail(email);
    }
}
