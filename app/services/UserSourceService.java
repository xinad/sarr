package services;


import com.google.inject.Inject;
import models.dataAccessObject.ISourceDao;
import models.dataAccessObject.IUserDao;
import models.dataAccessObject.IUserSourceDao;
import models.entity.Source;
import models.entity.User;

import java.util.List;

/**
 *
 */
public class UserSourceService implements IUserSourceService {


    private final IUserDao userDao;
    private final ISourceDao sourceDao;
    private final IUserSourceDao userSourceDao;

    @Inject
    public UserSourceService(IUserDao userDao, ISourceDao sourceDao, IUserSourceDao userSourceDao) {
        this.userDao = userDao;
        this.sourceDao = sourceDao;
        this.userSourceDao = userSourceDao;
    }

    @Override
    public List<Source> findAll() {
        return this.sourceDao.findAll();
    }

    @Override
    public Source findById(Long id) {
        return this.sourceDao.findById(id);
    }

    @Override
    public Source findByLink(String link) {
        return this.sourceDao.findByLink(link);
    }

    @Override
    public List<Source> findByTitle(String title) {
        return this.sourceDao.findByTitle(title);
    }

    @Override
    public Long addSource(String url) {
        return this.sourceDao.addSource(url);
    }

    @Override
    public void addSourceToUser(Long sourceId, Long userId) {
        Source source = this.sourceDao.findById(sourceId);
        User user = this.userDao.findById(userId);
        this.userSourceDao.addSourceToUser(source, user);
    }

    @Override
    public void dropSource(Long sourceId) {
        this.sourceDao.dropSource(sourceId);
    }

    @Override
    public void dropSourceFromUser(Long sourceId, Long userId) {
        this.userSourceDao.dropSourceFromUser(sourceId, userId);
    }

    @Override
    public List<Source> findAllSourcesForUser(Long userId) {
        return this.userSourceDao.findSourcesForUser(userId);
    }

}
