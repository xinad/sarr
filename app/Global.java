import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import models.dataAccessObject.*;
import models.dataAccessObject.ebean.*;
import play.Application;
import play.GlobalSettings;
import services.*;
import services.backgroundServices.*;

public class Global extends GlobalSettings {

    private Injector injector;

    @Override
    public void onStart(Application application) {
        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(IUserDao.class)                    .to(UserDao.class);
                bind(ISourceDao.class)                  .to(SourceDao.class);
                bind(IArticleDao.class)                 .to(ArticleDao.class);
                bind(IArticleParser.class)              .to(ArticleParser.class);
                bind(IUserSourceDao.class)              .to(UserSourceDao.class);
                bind(IUserArticleDao.class)             .to(UserArticleDao.class);
                bind(INewArticleBinder.class)           .to(NewArticleBinder.class);
                bind(IUserArticleTagDao.class)          .to(UserArticleTagDao.class);
                bind(IUserSourceService.class)          .to(UserSourceService.class);
                bind(IUserArticleService.class)         .to(UserArticleService.class);
                bind(IUserAuthenticationService.class)  .to(UserAuthenticationService.class);
            }

        });

        NewsCheckerService checkerService = new NewsCheckerService();
        checkerService.startParsing();
    }


    @Override
    public <T> T getControllerInstance(Class<T> aClass) throws Exception {
        return injector.getInstance(aClass);
    }
}