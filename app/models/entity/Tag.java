/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Entity for table with tags.
 * Every article can be tagged with simple name.
 * It's useful for filtering the articles.
 * The filter is based on these tags.
 *
 * @author Filip Vozár
 * @version 1.0
 * @since 1.0
 */
@Entity
public class Tag extends Model {

    @Id
    public Long id;

    @NotNull
    @Constraints.Required
    public String title;

    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
}
