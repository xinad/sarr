/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */
package models.entity;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Provide basic type for every source.
 * It can be RSS, Atom or custom type of source.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see Source
 * @since 1.0
 */
@Entity
public class SourceType extends Model {

    @Id
    public Long id;

    @NotNull
    @Constraints.Required
    public String title;

    public SourceType(String title) {
        this.title = title;
    }

    public static Finder<Long, SourceType> find = new Finder<>(Long.class, SourceType.class);
}
