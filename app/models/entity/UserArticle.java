/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Many-to-many relationship between User and his Articles.
 * This entity is used as joining point for two tables.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see User
 * @see Article
 * @since 1.0
 */
@Entity
public class UserArticle extends Model {

    @EmbeddedId
    public UserArticlePK pk;

    @JsonBackReference("user-article")
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    public User user;

    @JsonBackReference("user-article")
    @ManyToOne
    @JoinColumn(name = "article_id", insertable = false, updatable = false)
    public Article article;

    public boolean seen;

    public boolean starred;
}
