/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Join point for several tables.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see User
 * @see Article
 * @see Tag
 * @since 1.0
 */
@Entity
public class UserArticleTag extends Model {

    @EmbeddedId
    public UserArticleTagPK pk;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "article_id", insertable = false, updatable = false)
    public Article article;

    @ManyToOne
    @JoinColumn(name = "tag_id", insertable = false, updatable = false, nullable = true)
    public Tag tag;
}
