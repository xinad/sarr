/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Many-to-many relationship between User and his Source.
 * This entity is used as joining point for two tables.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see User
 * @see Source
 * @since 1.0
 */
@Entity
public class UserSource extends Model {

    @EmbeddedId
    public UserSourcePK pk;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    public User user;

    @ManyToOne
    @JoinColumn(name = "source_id", insertable = false, updatable = false)
    public Source source;

    public boolean seen;

    public static Finder<UserSourcePK, UserSource> find = new Finder<UserSourcePK, UserSource>(UserSourcePK.class, UserSource.class);
}
