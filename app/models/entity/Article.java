/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Entity describing basic unit of article.
 * There is basic info about article stored into database in case
 * of further operation with articles (sorting, tagging, etc.).
 *
 * @author Filip Vozár
 * @version 1.0
 * @since 1.0
 */
@Entity
public class Article extends Model {

    @Id
    public Long id;

    @NotNull
    @Constraints.Required
    public String title;

    @Column(length = 4096)
    public String description;

    public String author;

    @NotNull
    @Constraints.Required
    @Column(unique = true)
    public String link;

    public Date publishDate;

    @JsonManagedReference("source-article")
    @ManyToOne
    public Source source;

    @JsonBackReference("user-article")
    @OneToMany(mappedBy = "article")
    public List<UserArticle> userArticles = new ArrayList<>();

    public static Finder<Long, Article> find = new Finder<>(Long.class, Article.class);

    public Article(String title, String link) {
        this.title = title;
        this.link = link;
    }
}
