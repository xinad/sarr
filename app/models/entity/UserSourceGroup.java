/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Join point for several tables.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see User
 * @see Source
 * @see SourceGroup
 * @since 1.0
 */
@Entity
public class UserSourceGroup extends Model {

    @EmbeddedId
    UserSourceGroupPK pk;

    @ManyToOne
    @JoinColumn(name = "user_id")
    public User user;

    @ManyToOne
    @JoinColumn(name = "source_id")
    public Source source;

    @ManyToOne
    @JoinColumn(name = "group_id")
    public SourceGroup sourceGroup;
}
