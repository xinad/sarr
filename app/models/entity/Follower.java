/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Entity for 'follow' functionality.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see User
 * @since 1.0
 */
@Entity
public class Follower extends Model {

    @EmbeddedId
    public FollowerPK pk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "follower_id", insertable = false, updatable = false)
    public User follower;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "following_id", insertable = false, updatable = false)
    public User following;

}
