/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity describing basic unit of article.
 * There is basic info about article stored into database in case
 * of further operation with articles (sorting, taggind, etc.).
 *
 * @author Filip Vozár
 * @version 1.0
 * @since 1.0
 */
@Entity
public class Source extends Model {

    @Id
    public Long id;

    public String title;

    public String description;

    @Column(unique = true)
    public String link;

    public String uri;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    public Date publishDate;

    @OneToOne(cascade = CascadeType.ALL)
    public SourceType type;

    @JsonBackReference("source-article")
    @OneToMany
    public Set<Article> articles = new HashSet<>();

    @JsonBackReference("user-source")
    @OneToMany(mappedBy = "source")
    public Set<UserSource> userSources = new HashSet<>();

    @JsonBackReference("user-source-group")
    @OneToMany(mappedBy = "source")
    public Set<UserSourceGroup> userSourceGroups = new HashSet<>();

    public static Finder<Long, Source> find = new Finder<>(Long.class, Source.class);

}
