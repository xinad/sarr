/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;


import javax.persistence.Embeddable;

@Embeddable
public class UserArticleTagPK {

    public Long userId;

    public Long articleId;

    public Long tagId;

    public UserArticleTagPK(Long userId, Long articleId, Long tagId) {
        this.userId = userId;
        this.articleId = articleId;
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserArticleTagPK other = (UserArticleTagPK) obj;

        if ((this.userId == null) ? (other.userId != null) : !this.userId.equals(other.userId)) {
            return false;
        }
        if ((this.articleId == null) ? (other.articleId != null) : !this.articleId.equals(other.articleId)) {
            return false;
        }
        if ((this.tagId == null) ? (other.tagId != null) : !this.tagId.equals(other.tagId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 42;
        hash = 42 * hash + (this.userId != null ? this.userId.hashCode() : 0);
        hash = 42 * hash + (this.articleId != null ? this.articleId.hashCode() : 0);
        hash = 42 * hash + (this.tagId != null ? this.tagId.hashCode() : 0);
        return hash;
    }
}
