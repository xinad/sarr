/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Entity with basic information about each registered user.
 *
 * @author Filip Vozár
 * @version 1.0
 * @since 1.0
 */
@Entity
public class User extends Model {

    @Id
    public Long id;

    @Column(length = 50)
    public String firstName;

    @Column(length = 50)
    public String lastName;

    @NotNull
    @Constraints.Required
    @Constraints.Email
    @Column(length = 70, unique = true)
    public String email;

    @Column(length = 50)
    public String nick;

    @Column(length = 50)
    public String password;

    @Constraints.Min(value = 6)
    public int age;

    public User.Gender gender;

    @CreatedTimestamp
    public Timestamp registrationDate;

    @Version
    public Timestamp lastLogin;

    @JsonManagedReference("user-source")
    @OneToMany(mappedBy = "user")
    public Set<UserSource> userSources = new HashSet<>();

    @JsonManagedReference("user-source-group")
    @OneToMany(mappedBy = "user")
    public Set<UserSourceGroup> userSourceGroups = new HashSet<>();

    @JsonManagedReference("user-article")
    @OneToMany(mappedBy = "user")
    public List<UserArticle> userArticles = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    public List<UserArticleTag> userArticleTags = new ArrayList<>();

    public User(String email, String nick, String password) {
        this.email = email;
        this.nick = nick;
        this.password = password;
    }

    public static Finder<Long, User> find = new Finder<>(Long.class, User.class);

    public enum Gender {
        @EnumValue("M")
        MALE,

        @EnumValue("F")
        FEMALE
    }
}
