/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * Entity for table with user defined keywords.
 * Keywords are used to highlight words in titles and descriptions of articles.
 * Each user can define his own set of keywords.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see Source
 * @since 1.0
 */
@Entity
public class Keyword extends Model {

    @Id
    public Long id;

    @NotNull
    @Constraints.Required
    public String content;

    @OneToOne
    public User user;

    public static Finder<Long, Keyword> find = new Finder<>(Long.class, Keyword.class);
}
