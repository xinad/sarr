/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import javax.persistence.Embeddable;

@Embeddable
public class UserSourceGroupPK {

    public Long userId;

    public Long sourceId;

    public Long groupId;

    public UserSourceGroupPK(Long userId, Long sourceId, Long groupId) {
        this.userId = userId;
        this.sourceId = sourceId;
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserSourceGroupPK other = (UserSourceGroupPK) obj;

        if ((this.userId == null) ? (other.userId != null) : !this.userId.equals(other.userId)) {
            return false;
        }
        if ((this.sourceId == null) ? (other.sourceId != null) : !this.sourceId.equals(other.sourceId)) {
            return false;
        }
        if ((this.groupId == null) ? (other.groupId != null) : !this.groupId.equals(other.groupId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 69;
        hash = 79 * hash + (this.userId != null ? this.userId.hashCode() : 0);
        hash = 79 * hash + (this.sourceId != null ? this.sourceId.hashCode() : 0);
        hash = 79 * hash + (this.groupId != null ? this.groupId.hashCode() : 0);
        return hash;
    }
}
