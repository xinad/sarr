/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity for storing named group for specific source.
 * User can sort his list of channels into the named groups.
 * To do it, it's necessary to store group names into database.
 *
 * @author Filip Vozár
 * @version 1.0
 * @see Source
 * @since 1.0
 */
@Entity
public class SourceGroup extends Model {

    @Id
    public Long id;

    @NotNull
    @Constraints.Required
    public String path;

    @OneToMany(mappedBy = "sourceGroup")
    public Set<UserSourceGroup> userSourceGroups = new HashSet<>();
}
