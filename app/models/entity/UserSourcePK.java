/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserSourcePK implements Serializable {

    public Long userId;

    public Long sourceId;

    public UserSourcePK(Long userId, Long sourceId) {
        this.userId = userId;
        this.sourceId = sourceId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserSourcePK other = (UserSourcePK) obj;
        if ((this.userId == null) ? (other.userId != null) : !this.userId.equals(other.userId)) {
            return false;
        }
        if ((this.sourceId == null) ? (other.sourceId != null) : !this.sourceId.equals(other.sourceId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 64;
        hash = 32 * hash + (this.userId != null ? this.userId.hashCode() : 0);
        hash = 32 * hash + (this.sourceId != null ? this.sourceId.hashCode() : 0);
        return hash;
    }
}
