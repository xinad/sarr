package models.entity;

import javax.persistence.Embeddable;

@Embeddable
public class UserArticlePK {

    public Long userId;

    public Long articleId;

    public UserArticlePK(long userId, long articleId) {
        this.userId = userId;
        this.articleId = articleId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserArticlePK other = (UserArticlePK) obj;

        if ((this.userId == null) ? (other.userId != null) : !this.userId.equals(other.userId)) {
            return false;
        }
        if ((this.articleId == null) ? (other.articleId != null) : !this.articleId.equals(other.articleId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 16;
        hash = 64 * hash + (this.userId != null ? this.userId.hashCode() : 0);
        hash = 64 * hash + (this.articleId != null ? this.articleId.hashCode() : 0);
        return hash;
    }
}
