/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class FollowerPK implements Serializable {

    public Long followerId;

    public Long followingId;

    public FollowerPK(Long followerId, Long followingId) {
        this.followerId = followerId;
        this.followingId = followingId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final FollowerPK other = (FollowerPK) obj;
        if ((this.followerId == null) ? (other.followerId != null) : !this.followerId.equals(other.followerId)) {
            return false;
        }
        if ((this.followingId == null) ? (other.followingId != null) : !this.followingId.equals(other.followingId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.followerId != null ? this.followerId.hashCode() : 0);
        hash = 89 * hash + (this.followingId != null ? this.followingId.hashCode() : 0);
        return hash;
    }
}