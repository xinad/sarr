package models.dataAccessObject;


import models.entity.Article;
import models.entity.User;
import models.entity.UserArticle;

import java.util.List;
import java.util.Map;

public interface IUserArticleDao {

    /**
     * Set state of article as seen for user with his ID.
     *
     * @param userId    user ID to update article state for.
     * @param articleId ID of article to update.
     * @throws IllegalArgumentException if userId or articleId is incorrect, throw this exception.
     */
    public void markArticleAsReadForUser(Long userId, Long articleId) throws IllegalArgumentException;

    /**
     * Set state of article as starred (liked by user) for user.
     *
     * @param userId    user ID to update article state for.
     * @param articleId ID of article to update.
     * @throws IllegalArgumentException if userId or articleId is incorrect, throw this exception.
     */
    public void setStarToArticleForUser(Long userId, Long articleId, boolean isStarred) throws IllegalArgumentException;

    public List<UserArticle> getArticleStatesForUser(Long userId) ;

    public List<Article> getUnReadArticles(Long userId);

    public List<Article> getStarredArticles(Long userId);

    public Map<Long, Map<String, Boolean>> getStateForArticles(Long userId, List<Long> articleIDs);

    public void addArticleToUser(User user, Article article);
}
