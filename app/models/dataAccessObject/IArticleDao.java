/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.dataAccessObject;

import models.entity.Article;
import models.entity.Source;
import models.entity.User;

import java.util.List;

/**
 *
 */
public interface IArticleDao {

    /**
     * Get article with its id.
     *
     * @param id article id
     * @return article entity
     */
    public Article findById(Long id);

    /**
     * Find all articles
     *
     * @return list of all articles
     */
    public List<Article> findAll();

    /**
     * Find all articles for specific user
     *
     * @param user user object
     * @return list of all articles for user
     */
    public List<Article> findArticlesByUser(User user);

    /**
     * Find the latest saved article for user.
     *
     * @param user user object
     * @return the latest article
     */
    public Article findLatestArticleForUser(User user);


    /**
     * Find the latest saved article from source
     *
     * @param source source to find article from
     * @return article
     */
    public Article findLatestArticleFromSource(Source source);

    /**
     * Find article with id for specific user
     *
     * @param userId    user ID to search article for
     * @param articleId article ID to search
     * @return required article by its id
     */
    public Article findArticleForUser(Long userId, Long articleId);

}
