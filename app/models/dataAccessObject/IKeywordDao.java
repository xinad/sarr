package models.dataAccessObject;

import models.entity.Keyword;

import java.util.List;

/**
 *
 */
public interface IKeywordDao {

    public List<Keyword> findAll();

    public Keyword findById(Long id);
}
