package models.dataAccessObject;

import models.entity.Source;

import java.util.List;

/**
 *
 */
public interface ISourceDao {

    /**
     * Returns all source entities.
     *
     * @return List of source entities
     */
    public List<Source> findAll();

    /**
     * Returns source entity with given id.
     *
     * @param sourceId ID of source
     * @return Source entity
     */
    public Source findById(Long sourceId);

    public Source findByLink(String link);

    public List<Source> findByTitle(String title);

    public Long addSource(String url);

    public void dropSource(Long sourceId);
}
