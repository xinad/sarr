/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.dataAccessObject;

import models.entity.User;
import models.entity.UserSourceGroup;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: filip
 * Date: 11/30/13
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IUserDao {

    public List<User> findAll();

    public User findById(Long id);

    public Set<UserSourceGroup> getGroupsById(Long id);

    public User findByEmail(String email);
}
