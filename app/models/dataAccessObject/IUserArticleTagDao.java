package models.dataAccessObject;


import models.entity.Article;
import models.entity.Tag;
import models.entity.User;
import models.entity.UserArticleTag;

import java.util.List;
import java.util.Set;

public interface IUserArticleTagDao {

    public Set<String> findAllTagsForUser(Long userId);

    public void saveTag(Long userId, Long articleId, String tag);

    public List<UserArticleTag> findTagsToArticles(Long userId);

    public void removeAllTags(Long userId, Long articleId);

    public List<UserArticleTag> findArticlesWithTag(Long userId, Long tagId);

    public Tag findTagForUser(Long userId, String tag);
}
