package models.dataAccessObject;


import models.entity.Source;
import models.entity.User;

import java.util.List;

public interface IUserSourceDao {

    void dropSourceFromUser(Long sourceId, Long userId);

    void addSourceToUser(Source sourceId, User userId);

    List<Source> findSourcesForUser(Long userId);
}
