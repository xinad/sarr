package models.dataAccessObject.ebean;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.FetchConfig;
import models.dataAccessObject.IUserArticleDao;
import models.entity.Article;
import models.entity.User;
import models.entity.UserArticle;
import models.entity.UserArticlePK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserArticleDao implements IUserArticleDao {

    @Override
    public void markArticleAsReadForUser(Long userId, Long articleId) throws IllegalArgumentException {
        UserArticle userArticle = Ebean.find(UserArticle.class)
                .where().eq("user.id", userId).eq("article.id", articleId)
                .findUnique();

        if (userArticle == null)
            throw new IllegalArgumentException("Article with id " + articleId + " doesn't exsist for user with id " + userId);

        userArticle.seen = true;
        userArticle.update();
    }

    @Override
    public void setStarToArticleForUser(Long userId, Long articleId, boolean isStarred) {
        UserArticle userArticle = Ebean.find(UserArticle.class)
                .where().eq("user.id", userId).eq("article.id", articleId)
                .findUnique();

        if (userArticle == null)
            throw new IllegalArgumentException("Article with id " + articleId + " doesn't exsist for user with id " + userId);

        userArticle.starred = isStarred;
        userArticle.update();
    }

    @Override
    public List<UserArticle> getArticleStatesForUser(Long userId) {
        return Ebean.find(UserArticle.class)
                .where().eq("user.id", userId)
                .findList();
    }

    @Override
    public List<Article> getUnReadArticles(Long userId) {
        List<UserArticle> userArticles = Ebean.find(UserArticle.class)
                .where()
                .eq("user.id", userId)
                .eq("seen", false)
                .findList();

        List<Article> articles = new ArrayList<>();
        for (UserArticle article : userArticles) {
            articles.add(article.article);
        }

        return articles;
    }

    @Override
    public List<Article> getStarredArticles(Long userId) {
        List<UserArticle> userArticles = Ebean.find(UserArticle.class)
                .where()
                .eq("user.id", userId)
                .eq("starred", true)
                .findList();

        List<Article> articles = new ArrayList<>();
        for (UserArticle article : userArticles) {
            articles.add(article.article);
        }

        return articles;
    }

    @Override
    public Map<Long, Map<String, Boolean>> getStateForArticles(Long userId, List<Long> articleIDs) {
        List<UserArticle> articles = Ebean.find(UserArticle.class)
                .where()
                .eq("user.id", userId)
                .in("article.id", articleIDs)
                .findList();

        Map<Long, Map<String, Boolean>> states = new HashMap<>();

        for (UserArticle article : articles) {
            Map<String, Boolean> state = new HashMap<>();
            state.put("seen", article.seen);
            state.put("starred", article.starred);

            states.put(article.article.id, state);
        }

        return states;
    }

    @Override
    public void addArticleToUser(User user, Article article) {
        UserArticle userArticle = new UserArticle();
        userArticle.user = user;
        userArticle.article = article;
        userArticle.seen = false;
        userArticle.starred = false;
        userArticle.pk = new UserArticlePK(user.id, article.id);
        userArticle.save();
    }
}
