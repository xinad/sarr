/*
 * This file is part of the SARR (Simply Awesome RSS Reader) Project
 *
 * Copyright (c) 2013 Lukáš Melega, Lukáš Mitro and Filip Vozár
 */

package models.dataAccessObject.ebean;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.FetchConfig;
import models.dataAccessObject.IUserDao;
import models.entity.User;
import models.entity.UserSourceGroup;

import java.util.List;
import java.util.Set;

/**
 *
 */
public class UserDao implements IUserDao {

    public List<User> findAll() {
        return Ebean.find(User.class)
                .fetch("userSources", new FetchConfig().query())
                .fetch("userArticles", new FetchConfig().lazy())
                .fetch("userSourceGroups", new FetchConfig().query())
                .findList();
    }


    public User findById(Long id) {

        return Ebean.find(User.class)
                .setId(id)
                .fetch("userSources")
                .fetch("userArticles")
                .fetch("userSourceGroups")
                .findUnique();
    }

    public Set<UserSourceGroup> getGroupsById(Long id)
    {
       return Ebean.find(User.class)
                 .setId(id)
                 .fetch("userSourceGroups")
                 .fetch("userSourceGroups.source")
                 .fetch("userSourceGroups.sourceGroup").orderBy("userSourceGroups.sourceGroup.path")
                 .findUnique().userSourceGroups;

    }

    public User findByEmail(String email)
    {
        return Ebean.find(User.class).
                where().
                eq("email", email).
                findUnique();
    }
}
