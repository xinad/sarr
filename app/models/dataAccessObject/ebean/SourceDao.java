package models.dataAccessObject.ebean;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.FetchConfig;
import models.dataAccessObject.ISourceDao;
import models.entity.Source;

import java.util.List;

public class SourceDao implements ISourceDao {

    @Override
    public List<Source> findAll() {
        return Ebean.find(Source.class)
                .fetch("type")
                .fetch("articles", new FetchConfig().query())
                .findList();
    }

    @Override
    public Source findById(Long id) {
        return Ebean.find(Source.class)
                .setId(id)
                .fetch("type")
                .fetch("articles", new FetchConfig().query())
                .findUnique();
    }

    @Override
    public Source findByLink(String link) {
        return Ebean.find(Source.class)
                .where().eq("link", link)
                .findUnique();
    }

    @Override
    public List<Source> findByTitle(String title) {
        return Ebean.find(Source.class)
                .where().ilike("title", "%" + title + "%").findList();
    }

    @Override
    public Long addSource(String url) {
        Source source = Ebean.find(Source.class)
                .where().eq("link", url).findUnique();

        if (source == null) {
            source = new Source();
            source.link = url;
            source.save();
        }

        return source.id;
    }

    @Override
    public void dropSource(Long sourceId) {
        Source.find.byId(sourceId).delete();
    }
}
