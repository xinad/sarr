package models.dataAccessObject.ebean;

import com.avaje.ebean.Ebean;
import models.dataAccessObject.IUserSourceDao;
import models.entity.Source;
import models.entity.User;
import models.entity.UserSource;
import models.entity.UserSourcePK;

import java.util.ArrayList;
import java.util.List;


public class UserSourceDao implements IUserSourceDao {

    @Override
    public void addSourceToUser(Source source, User user) {
        UserSource userSource = new UserSource();
        userSource.user = user;
        userSource.source = source;
        userSource.seen = false;
        userSource.pk = new UserSourcePK(user.id, source.id);
        userSource.save();
    }

    @Override
    public List<Source> findSourcesForUser(Long userId) {
        List<UserSource> userSources = Ebean.find(UserSource.class)
                .where().eq("user.id", userId)
                .findList();

        List<Source> sources = new ArrayList<>();

        if (userSources != null) {
            for (UserSource userSource : userSources) {
                sources.add(userSource.source);
            }
        }
        return sources;
    }

    @Override
    public void dropSourceFromUser(Long sourceId, Long userId) {
        UserSource userSource = Ebean.find(UserSource.class)
                .where()
                .eq("user.id", userId)
                .eq("source.id", sourceId)
                .findUnique();

        if (userSource != null) {
            userSource.delete();
        }
    }
}
