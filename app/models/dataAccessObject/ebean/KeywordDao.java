package models.dataAccessObject.ebean;

import com.avaje.ebean.Ebean;
import models.dataAccessObject.IKeywordDao;
import models.entity.Keyword;

import java.util.List;

public class KeywordDao implements IKeywordDao {

    @Override
    public List<Keyword> findAll() {
        return Ebean.find(Keyword.class)
                .fetch("user")
                .findList();
    }

    @Override
    public Keyword findById(Long id) {
        return Ebean.find(Keyword.class)
                .setId(id)
                .fetch("user")
                .findUnique();
    }
}
