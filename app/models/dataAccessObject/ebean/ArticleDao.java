package models.dataAccessObject.ebean;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.FetchConfig;
import models.dataAccessObject.IArticleDao;
import models.entity.Article;
import models.entity.Source;
import models.entity.User;

import java.util.List;

/**
 *
 */
public class ArticleDao implements IArticleDao {

    /**
     * Get article with its id.
     *
     * @param id article id
     * @return article entity
     */
    @Override
    public Article findById(Long id) {
        return Ebean.find(Article.class)
                .fetch("source", new FetchConfig().query())
                .setId(id)
                .findUnique();
    }

    /**
     * Find all articles
     *
     * @return list of all articles
     */
    @Override
    public List<Article> findAll() {
        return Ebean.find(Article.class)
                .fetch("source", new FetchConfig().query()).findList();
    }

    /**
     * Find all articles for user with given id.
     *
     * @param user User
     * @return List of articles for user
     */
    @Override
    public List<Article> findArticlesByUser(User user) {
        return Ebean.find(Article.class)
                .fetch("userArticles", new FetchConfig().query())
                .fetch("userArticles.user")
                .where().eq("userArticles.user.id", user.id)
                .findList();
    }

    @Override
    public Article findLatestArticleForUser(User user) {
        return Ebean.find(Article.class)
                .fetch("userArticles", new FetchConfig().query())
                .fetch("userArticles.user")
                .where().eq("userArticles.user.id", user.id)
                .orderBy().desc("publishDate")
                .orderBy().desc("id")
                .setMaxRows(1)
                .findUnique();
    }

    public Article findLatestArticleFromSource(Source source) {
        return Ebean.find(Article.class)
                .where().eq("source.id", source.id)
                .orderBy().desc("id")
                .setMaxRows(1)
                .findUnique();
    }

    @Override
    public Article findArticleForUser(Long userId, Long articleId) {
        return Ebean.find(Article.class)
                .setId(articleId)
                .fetch("userArticles", new FetchConfig().query())
                .fetch("userArticles.user")
                .where().eq("userArticles.user.id", userId)
                .findUnique();
    }

}
