package models.dataAccessObject.ebean;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.FetchConfig;
import controllers.routes;
import models.dataAccessObject.IUserArticleTagDao;
import models.entity.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class UserArticleTagDao implements IUserArticleTagDao {

    @Override
    public Set<String> findAllTagsForUser(Long userId) {
        List<UserArticleTag> userArticleTagList = Ebean.find(UserArticleTag.class)
                .fetch("tag")
                .where().eq("user.id", userId).findList();

        Set<String> tags = new HashSet<>();
        for (UserArticleTag userArticleTag : userArticleTagList) {
            tags.add(userArticleTag.tag.title);
        }

        return tags;
    }

    @Override
    public void saveTag(Long userId, Long articleId, String tag) {

        User user = Ebean.find(User.class).setId(userId).findUnique();
        Article article = Ebean.find(Article.class).setId(articleId).findUnique();

        Tag newTag = Tag.find.where().eq("title", tag).findUnique();
        if (newTag == null) {
            newTag = new Tag();
            newTag.title = tag;
            newTag.save();
        }

        UserArticleTag userArticleTag = new UserArticleTag();
        userArticleTag.user = user;
        userArticleTag.article = article;
        userArticleTag.tag = newTag;
        userArticleTag.pk = new UserArticleTagPK(userId, articleId, newTag.id);
        userArticleTag.save();

        user.userArticleTags.add(userArticleTag);

        user.saveManyToManyAssociations("userArticleTags");

        user.save();
    }

    @Override
    public List<UserArticleTag> findTagsToArticles(Long userId) {
        return Ebean.find(UserArticleTag.class)
                .fetch("article")
                .fetch("tag")
                .where().eq("user.id", userId).findList();
    }

    @Override
    public void removeAllTags(Long userId, Long articleId) {
        List<UserArticleTag> tagList = Ebean.find(UserArticleTag.class)
                .where()
                .eq("user.id", userId)
                .eq("article.id", articleId)
                .findList();

        List<String> referenceList = new ArrayList<>();
        for (UserArticleTag tag : tagList) {
            referenceList.add(tag.tag.id.toString());
            tag.delete();
        }

/*
        List<Tag> tags = Ebean.find(Tag.class).where().idIn(referenceList).findList();
        for (Tag tag : tags) {
            tag.delete();

        }*/
    }

    @Override
    public List<UserArticleTag> findArticlesWithTag(Long userId, Long tagId) {
        return Ebean.find(UserArticleTag.class)
                .where()
                .eq("user.id", userId)
                .eq("tag.id", tagId)
                .findList();
    }

    @Override
    public Tag findTagForUser(Long userId, String tag) {
        UserArticleTag unique = Ebean.find(UserArticleTag.class)
                .where()
                .eq("user.id", userId)
                .eq("tag.title", tag)
                .setMaxRows(1).findUnique();

        return unique == null ? null : unique.tag;
    }
}
