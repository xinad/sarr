import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "SARR"
  val appVersion = "1.0-SNAPSHOT"
  
  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    javaJpa,
    "org.jumpmind.symmetric.jdbc" % "mariadb-java-client" % "1.1.1",
    "org.jumpmind.symmetric.jdbc" % "mariadb-java-client" % "1.1.1" % "test",
    "org.dbunit"                  % "dbunit"              % "2.4.9" % "test",
    "rome"                        % "rome"                % "1.0",
    "jdom"                        % "jdom"                % "1.0",
    "securesocial"                %% "securesocial"       % "2.1.2",
    "com.google.inject"           % "guice"               % "4.0-beta",
    "com.fasterxml.jackson.core"  % "jackson-databind"    % "2.3.0",
    "com.fasterxml.jackson.core"  % "jackson-annotations" % "2.3.0",
    "com.fasterxml.jackson.core"  % "jackson-core"        % "2.3.0"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
    // resolvers += "JBoss repository" at "https://repository.jboss.org/nexus/content/repositories/",
    // resolvers += "Scala-Tools Maven2 Snapshots Repository" at "http://scala-tools.org/repo-snapshots"
    resolvers += Resolver.url("sbt-plugin-releases", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
	)

}
